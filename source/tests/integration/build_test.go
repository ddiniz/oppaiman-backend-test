package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/build"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/build_repository"
	"oppaiman-backend-test/source/services/build_service"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockBuild(num int) *build.Build {
	return &build.Build{
		Name:    fmt.Sprintf("Name%d", num),
		Version: fmt.Sprintf("Ver%d", num),
		File:    fmt.Sprintf("File%d", num),
	}
}
func mockBuildRequest(num int) *build.BuildRequest {
	return &build.BuildRequest{
		Name:    fmt.Sprintf("Name%d", num),
		Version: fmt.Sprintf("Ver%d", num),
		File:    fmt.Sprintf("File%d", num),
	}
}

// TODO:extract paths for better test handling and ease of use
func TestBuildsPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/builds_dev").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/builds_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/builds_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindBuildById(t *testing.T) {
	RunTest(func() {
		obj := mockBuild(1)
		build_repository.CreateBuild(obj)

		emptyRest().
			GET(fmt.Sprintf("/builds/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &build.Build{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Name, gameResponse.Name)
				assert.Equal(t, obj.File, gameResponse.File)
			})
	})
}

func TestFindBuildByIdNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/builds/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindAllBuilds(t *testing.T) {
	RunTest(func() {
		obj1 := mockBuild(1)
		obj2 := mockBuild(2)
		build_repository.CreateBuild(obj1)
		build_repository.CreateBuild(obj2)

		emptyRest().
			GET("/builds").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []build.Build{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, obj1.Name, gameResponse[0].Name)
				assert.Equal(t, obj1.File, gameResponse[0].File)

				assert.Equal(t, obj2.Name, gameResponse[1].Name)
				assert.Equal(t, obj2.File, gameResponse[1].File)
			})
	})
}

func TestFindAllBuildsEmpty(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/builds").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostBuild(t *testing.T) {
	RunTest(func() {
		requestBody := mockBuildRequest(1)

		baseRest(u.DEVELOPER).
			POST("/builds_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, build_repository.ExistsBuildByName(requestBody.Name))
			})
	})
}

func TestPostBuildWithExistingName(t *testing.T) {
	RunTest(func() {
		obj := mockBuild(1)
		build_repository.CreateBuild(obj)

		requestBody := mockBuildRequest(1)

		baseRest(u.DEVELOPER).
			POST("/builds_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(build_service.ERR_BUILD_DUPLICATE_NAME, obj.Name), response.Body.String())
			})
	})
}
func TestPostBuildWithExistingVersion(t *testing.T) {
	RunTest(func() {
		obj := mockBuild(1)
		build_repository.CreateBuild(obj)

		requestBody := mockBuildRequest(1)
		requestBody.Name = "Name2"

		baseRest(u.DEVELOPER).
			POST("/builds_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(build_service.ERR_BUILD_DUPLICATE_VERSION, obj.Version), response.Body.String())
			})
	})
}

func TestUpdateBuild(t *testing.T) {
	RunTest(func() {
		obj := mockBuild(1)
		build_repository.CreateBuild(obj)

		requestBody := mockBuildRequest(1)

		baseRest(u.DEVELOPER).
			PUT(fmt.Sprintf("/builds_dev/%d", obj.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				buildAfterUpdate, _ := build_repository.FindBuildById(obj.ID)

				assert.Equal(t, obj.ID, buildAfterUpdate.ID)
				assert.Equal(t, obj.Name, buildAfterUpdate.Name)
				assert.Equal(t, obj.File, buildAfterUpdate.File)
			})
	})
}

func TestUpdateBuildNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockBuildRequest(1)

		baseRest(u.DEVELOPER).
			PUT("/builds/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeleteBuild(t *testing.T) {
	RunTest(func() {
		obj := mockBuild(1)
		build_repository.CreateBuild(obj)

		baseRest(u.DEVELOPER).
			DELETE(fmt.Sprintf("/builds_dev/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				game, err := build_repository.FindBuildById(obj.ID)
				assert.Empty(t, game)
				assert.Error(t, err)
			})
	})
}

func TestDeleteBuildNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.DEVELOPER).
			DELETE("/builds/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
