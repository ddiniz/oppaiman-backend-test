package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/image"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/image_repository"
	"oppaiman-backend-test/source/services/image_service"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockImage(num int) *image.Image {
	return &image.Image{
		Name: fmt.Sprintf("Name%d", num),
		Src:  fmt.Sprintf("base64image%d", num),
	}
}
func mockImageRequest(num int) *image.ImageRequest {
	return &image.ImageRequest{
		Name: fmt.Sprintf("Name%d", num),
		Src:  fmt.Sprintf("base64image%d", num),
	}
}

// TODO:extract paths for better test handling and ease of use
func TestImagesPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/images_dev").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/images_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/images_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindImageById(t *testing.T) {
	RunTest(func() {
		obj := mockImage(1)
		image_repository.CreateImage(obj)

		emptyRest().
			GET(fmt.Sprintf("/images/%s", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &image.Image{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Name, gameResponse.Name)
				assert.Equal(t, obj.Src, gameResponse.Src)
			})
	})
}

func TestFindImageByIdNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/images/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindAllImages(t *testing.T) {
	RunTest(func() {
		obj1 := mockImage(1)
		obj2 := mockImage(2)
		image_repository.CreateImage(obj1)
		image_repository.CreateImage(obj2)

		emptyRest().
			GET("/images").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []image.Image{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, obj1.Name, gameResponse[0].Name)
				assert.Equal(t, obj1.Src, gameResponse[0].Src)

				assert.Equal(t, obj2.Name, gameResponse[1].Name)
				assert.Equal(t, obj2.Src, gameResponse[1].Src)
			})
	})
}

func TestFindAllImagesEmpty(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/images").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostImage(t *testing.T) {
	RunTest(func() {
		requestBody := mockImageRequest(1)

		baseRest(u.DEVELOPER).
			POST("/images_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, image_repository.ExistsImageByName(requestBody.Name))
			})
	})
}

func TestPostImageWithExistingTitle(t *testing.T) {
	RunTest(func() {
		obj := mockImage(1)
		image_repository.CreateImage(obj)

		requestBody := mockImageRequest(1)

		baseRest(u.DEVELOPER).
			POST("/images_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(image_service.ERR_IMAGE_DUPLICATE_NAME, obj.Name), response.Body.String())
			})
	})
}

func TestUpdateImage(t *testing.T) {
	RunTest(func() {
		obj := mockImage(1)
		image_repository.CreateImage(obj)

		requestBody := mockImageRequest(1)

		baseRest(u.DEVELOPER).
			PUT(fmt.Sprintf("/images_dev/%s", obj.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				imageAfterUpdate, _ := image_repository.FindImageById(obj.ID)

				assert.Equal(t, obj.ID, imageAfterUpdate.ID)
				assert.Equal(t, obj.Name, imageAfterUpdate.Name)
				assert.Equal(t, obj.Src, imageAfterUpdate.Src)
			})
	})
}

func TestUpdateImageNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockImageRequest(1)

		baseRest(u.DEVELOPER).
			PUT("/images/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeleteImage(t *testing.T) {
	RunTest(func() {
		obj := mockImage(1)
		image_repository.CreateImage(obj)

		baseRest(u.DEVELOPER).
			DELETE(fmt.Sprintf("/images_dev/%s", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				game, err := image_repository.FindImageById(obj.ID)
				assert.Empty(t, game)
				assert.Error(t, err)
			})
	})
}

func TestDeleteImageNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.DEVELOPER).
			DELETE("/images/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
