package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/genre"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/genre_repository"
	"oppaiman-backend-test/source/services/genre_service"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockGenre(num int) *genre.Genre {
	return &genre.Genre{
		Name:        fmt.Sprintf("Name%d", num),
		Description: fmt.Sprintf("Description%d", num),
	}
}

func mockGenreRequest(num int) *genre.GenreRequest {
	return &genre.GenreRequest{
		Name:        fmt.Sprintf("Name%d", num),
		Description: fmt.Sprintf("Description%d", num),
	}
}

// TODO:extract paths for better test handling and ease of use
func TestGenresPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/genres_admin").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/genres_admin/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/genres_admin/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindGenreById(t *testing.T) {
	RunTest(func() {
		obj := mockGenre(1)
		genre_repository.CreateGenre(obj)

		emptyRest().
			GET(fmt.Sprintf("/genres/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &genre.Genre{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Name, gameResponse.Name)
				assert.Equal(t, obj.Description, gameResponse.Description)
			})
	})
}

func TestFindGenreByIdNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/genres/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindAllGenres(t *testing.T) {
	RunTest(func() {
		obj1 := mockGenre(1)
		obj2 := mockGenre(2)
		genre_repository.CreateGenre(obj1)
		genre_repository.CreateGenre(obj2)

		emptyRest().
			GET("/genres").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []genre.Genre{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, obj1.Name, gameResponse[0].Name)
				assert.Equal(t, obj1.Description, gameResponse[0].Description)

				assert.Equal(t, obj2.Name, gameResponse[1].Name)
				assert.Equal(t, obj2.Description, gameResponse[1].Description)
			})
	})
}

func TestFindAllGenresEmpty(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/genres").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostGenre(t *testing.T) {
	RunTest(func() {
		requestBody := mockGenreRequest(1)

		baseRest(u.ADMIN).
			POST("/genres_admin").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, genre_repository.ExistsGenreByName(requestBody.Name))
			})
	})
}

func TestPostGenreWithExistingName(t *testing.T) {
	RunTest(func() {
		obj := mockGenre(1)
		genre_repository.CreateGenre(obj)

		requestBody := mockGenreRequest(1)

		baseRest(u.ADMIN).
			POST("/genres_admin").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(genre_service.ERR_GENRE_DUPLICATE_NAME, obj.Name), response.Body.String())
			})
	})
}

func TestUpdateGenre(t *testing.T) {
	RunTest(func() {
		obj := mockGenre(1)
		genre_repository.CreateGenre(obj)

		requestBody := mockGenreRequest(1)

		baseRest(u.ADMIN).
			PUT(fmt.Sprintf("/genres_admin/%d", obj.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				genreAfterUpdate, _ := genre_repository.FindGenreById(obj.ID)

				assert.Equal(t, obj.ID, genreAfterUpdate.ID)
				assert.Equal(t, obj.Name, genreAfterUpdate.Name)
				assert.Equal(t, obj.Description, genreAfterUpdate.Description)
			})
	})
}

func TestUpdateGenreNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockGenreRequest(1)

		baseRest(u.ADMIN).
			PUT("/genres/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeleteGenre(t *testing.T) {
	RunTest(func() {
		obj := mockGenre(1)
		genre_repository.CreateGenre(obj)

		baseRest(u.ADMIN).
			DELETE(fmt.Sprintf("/genres_admin/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				game, err := genre_repository.FindGenreById(obj.ID)
				assert.Empty(t, game)
				assert.Error(t, err)
			})
	})
}

func TestDeleteGenreNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			DELETE("/genres/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
