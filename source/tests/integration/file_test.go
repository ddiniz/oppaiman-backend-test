package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/image"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/image_repository"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockFile(num int) *image.Image {
	return &image.Image{
		Name: fmt.Sprintf("Name%d", num),
		Src:  fmt.Sprintf("base64image%d", num),
	}
}
func mockImageFile(num int) *image.ImageRequest {
	return &image.ImageRequest{
		Name: fmt.Sprintf("Name%d", num),
		Src:  fmt.Sprintf("base64image%d", num),
	}
}

// TODO:extract paths for better test handling and ease of use
func TestFilesPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/files_dev").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindFileById(t *testing.T) {
	RunTest(func() {
		obj := mockImage(1)
		image_repository.CreateImage(obj)

		emptyRest().
			GET(fmt.Sprintf("/images/%s", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &image.Image{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Name, gameResponse.Name)
				assert.Equal(t, obj.Src, gameResponse.Src)
			})
	})
}
