package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/pagination"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/game_repository"
	"oppaiman-backend-test/source/repository/user_repository"
	"oppaiman-backend-test/source/services/user_service"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockUser(num int, role u.Roles) *u.User {
	return &u.User{
		Email:    fmt.Sprintf("Email%d@email.com", num),
		Username: fmt.Sprintf("Username%d", num),
		Password: fmt.Sprintf("Password%d", num),
		Role:     role,
	}
}

func mockUserRequest(num int, role u.Roles) *u.UserRequest {
	return &u.UserRequest{
		Email:    fmt.Sprintf("Email%d@email.com", num),
		Username: fmt.Sprintf("Username%d", num),
		Password: fmt.Sprintf("Password%d", num),
		Role:     role,
	}
}

func TestUsersPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL, u.DEVELOPER}
		for _, role := range roles {
			baseRest(role).
				GET("/users_admin").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				GET("/users_admin/paginated").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				GET("/users_admin/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				GET("/users_admin/username/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				GET("/users_admin/email/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/users_admin/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/users_admin/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindUserById(t *testing.T) {
	RunTest(func() {
		newUser := mockUser(1, u.NORMAL)
		user_repository.CreateUser(newUser)

		baseRest(u.ADMIN).
			GET(fmt.Sprintf("/users_admin/%s", newUser.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				userResponse := &u.UserResponse{}
				json.Unmarshal(response.Body.Bytes(), userResponse)
				assert.Equal(t, http.StatusOK, response.Code)
				assert.NotEmpty(t, userResponse.ID)
				assert.Equal(t, newUser.Email, userResponse.Email)
				assert.Equal(t, newUser.Username, userResponse.Username)
				assert.Equal(t, newUser.Role, userResponse.Role)
				assert.NotEmpty(t, userResponse.CreatedAt)
				assert.NotEmpty(t, userResponse.UpdatedAt)
				assert.Empty(t, userResponse.DeletedAt)
			})
	})
}

func TestFindUserByEmail(t *testing.T) {
	RunTest(func() {
		newUser := mockUser(1, u.NORMAL)
		user_repository.CreateUser(newUser)

		baseRest(u.ADMIN).
			GET(fmt.Sprintf("/users_admin/email/%s", newUser.Email)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				userResponse := &u.UserResponse{}
				json.Unmarshal(response.Body.Bytes(), userResponse)
				assert.Equal(t, http.StatusOK, response.Code)
				assert.NotEmpty(t, userResponse.ID)
				assert.Equal(t, newUser.Email, userResponse.Email)
				assert.Equal(t, newUser.Username, userResponse.Username)
				assert.Equal(t, newUser.Role, userResponse.Role)
				assert.NotEmpty(t, userResponse.CreatedAt)
				assert.NotEmpty(t, userResponse.UpdatedAt)
				assert.Empty(t, userResponse.DeletedAt)
			})
	})
}

func TestFindUserByUsername(t *testing.T) {
	RunTest(func() {
		newUser := mockUser(1, u.NORMAL)
		user_repository.CreateUser(newUser)

		baseRest(u.ADMIN).
			GET(fmt.Sprintf("/users_admin/username/%s", newUser.Username)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				userResponse := &u.UserResponse{}
				json.Unmarshal(response.Body.Bytes(), userResponse)
				assert.Equal(t, http.StatusOK, response.Code)
				assert.NotEmpty(t, userResponse.ID)
				assert.Equal(t, newUser.Email, userResponse.Email)
				assert.Equal(t, newUser.Username, userResponse.Username)
				assert.Equal(t, newUser.Role, userResponse.Role)
				assert.NotEmpty(t, userResponse.CreatedAt)
				assert.NotEmpty(t, userResponse.UpdatedAt)
				assert.Empty(t, userResponse.DeletedAt)
			})
	})
}

func TestFindUserByIdNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			GET("/users_admin/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
func TestFindUserByEmailNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			GET("/users_admin/email/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
func TestFindUserByUsernameNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			GET("/users_admin/username/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindUsersPaginatedDefaultValues(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			GET("/users_admin/paginated").
			SetQuery(gofight.H{
				"page":  "0",
				"limit": "0",
			}).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(0), paginatorResponse.TotalPages)
				assert.Equal(t, 0, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindUsersPaginated(t *testing.T) {
	RunTest(func() {
		for i := 0; i < 30; i++ {
			user_repository.CreateUser(mockUser(i, u.NORMAL))
		}

		baseRest(u.ADMIN).
			GET("/users_admin/paginated").
			SetQuery(gofight.H{
				"page":  "1",
				"limit": "10",
			}).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(3), paginatorResponse.TotalPages)
				assert.Equal(t, 10, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindAllUsers(t *testing.T) {
	RunTest(func() {
		user1 := mockUser(1, u.NORMAL)
		user2 := mockUser(2, u.NORMAL)
		user_repository.CreateUser(user1)

		user_repository.CreateUser(user2)

		baseRest(u.ADMIN).
			GET("/users_admin").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				userResponse := []u.UserResponse{}
				json.Unmarshal(response.Body.Bytes(), &userResponse)
				assert.Equal(t, http.StatusOK, response.Code)
				assert.NotEmpty(t, userResponse[0].ID)
				assert.Equal(t, user1.Email, userResponse[0].Email)
				assert.Equal(t, user1.Username, userResponse[0].Username)
				assert.Equal(t, user1.Role, userResponse[0].Role)
				assert.NotEmpty(t, userResponse[0].CreatedAt)
				assert.NotEmpty(t, userResponse[0].UpdatedAt)
				assert.Empty(t, userResponse[0].DeletedAt)

				assert.NotEmpty(t, userResponse[1].ID)
				assert.Equal(t, user2.Email, userResponse[1].Email)
				assert.Equal(t, user2.Username, userResponse[1].Username)
				assert.Equal(t, user2.Role, userResponse[1].Role)
				assert.NotEmpty(t, userResponse[1].CreatedAt)
				assert.NotEmpty(t, userResponse[1].UpdatedAt)
				assert.Empty(t, userResponse[1].DeletedAt)
			})
	})
}

func TestFindAllUsersEmpty(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			GET("/users_admin").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostUser(t *testing.T) {
	RunTest(func() {
		requestBody := mockUserRequest(1, u.NORMAL)

		emptyRest().
			POST("/users").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, user_repository.ExistsUserByEmail(requestBody.Email))
				assert.True(t, user_repository.ExistsUserByUsername(requestBody.Username))
				user, _ := user_repository.FindUserByEmail(requestBody.Email)
				assert.NotEmpty(t, user.ID)
				assert.Equal(t, user.Email, requestBody.Email)
				assert.Equal(t, user.Username, requestBody.Username)
				assert.Equal(t, user.Role, requestBody.Role)
				assert.NotEmpty(t, user.CreatedAt)
				assert.NotEmpty(t, user.UpdatedAt)
				assert.Empty(t, user.DeletedAt)
			})
	})
}

func TestPostUserDeveloper(t *testing.T) {
	RunTest(func() {
		requestBody := mockUserRequest(1, u.DEVELOPER)

		emptyRest().
			POST("/users").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, user_repository.ExistsUserByEmail(requestBody.Email))
				assert.True(t, user_repository.ExistsUserByUsername(requestBody.Username))
				user, _ := user_repository.FindUserByEmail(requestBody.Email)
				assert.NotEmpty(t, user.ID)
				assert.Equal(t, user.Email, requestBody.Email)
				assert.Equal(t, user.Username, requestBody.Username)
				assert.Equal(t, user.Role, requestBody.Role)
				assert.NotEmpty(t, user.CreatedAt)
				assert.NotEmpty(t, user.UpdatedAt)
				assert.Empty(t, user.DeletedAt)
			})
	})
}

func TestPostUserAdminFails(t *testing.T) {
	RunTest(func() {
		requestBody := mockUserRequest(1, u.ADMIN)

		emptyRest().
			POST("/users").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			})
	})
}

func TestPostUserWithExistingUsername(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)

		requestBody := mockUserRequest(1, u.NORMAL)

		emptyRest().
			POST("/users").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(user_service.ERR_USER_DUPLICATE_USERNAME, user.Username), response.Body.String())
			})
	})
}
func TestPostUserWithExistingEmail(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)

		requestBody := mockUserRequest(1, u.NORMAL)
		requestBody.Username = "Username2"
		emptyRest().
			POST("/users").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(user_service.ERR_USER_DUPLICATE_EMAIL, user.Email), response.Body.String())
			})
	})
}

func TestUpdateUser(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)

		requestBody := mockUserRequest(1, u.NORMAL)

		baseRest(u.ADMIN).
			PUT(fmt.Sprintf("/users_admin/%s", user.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				userAfterUpdate, _ := user_repository.FindUserById(user.ID)

				assert.NotEmpty(t, userAfterUpdate.ID)
				assert.Equal(t, user.ID, userAfterUpdate.ID)
				assert.Equal(t, requestBody.Email, userAfterUpdate.Email)
				assert.NotEqual(t, requestBody.Password, userAfterUpdate.Password)
				assert.NotEqual(t, user.Password, userAfterUpdate.Password)
				assert.Equal(t, requestBody.Role, userAfterUpdate.Role)
				assert.Equal(t, requestBody.Username, userAfterUpdate.Username)
			})
	})
}

func TestUpdateUserNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockUserRequest(1, u.NORMAL)

		baseRest(u.ADMIN).
			PUT("/users_admin/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeleteUser(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)

		baseRest(u.ADMIN).
			DELETE(fmt.Sprintf("/users_admin/%s", user.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				user, err := user_repository.FindUserById(user.ID)
				assert.Empty(t, user)
				assert.Error(t, err)
			})
	})
}

func TestDeleteUserNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.ADMIN).
			DELETE("/users_admin/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestUpdateSelf(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)
		requestBody := mockUserRequest(1, u.NORMAL)

		selfRest(user).
			PUT("/users_self").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				userAfterUpdate, _ := user_repository.FindUserById(user.ID)

				assert.NotEmpty(t, userAfterUpdate.ID)
				assert.Equal(t, user.ID, userAfterUpdate.ID)
				assert.Equal(t, requestBody.Email, userAfterUpdate.Email)
				assert.NotEqual(t, requestBody.Password, userAfterUpdate.Password)
				assert.NotEqual(t, user.Password, userAfterUpdate.Password)
				assert.Equal(t, requestBody.Role, userAfterUpdate.Role)
				assert.Equal(t, requestBody.Username, userAfterUpdate.Username)
			})
	})
}

func TestUpdateSelfCantBecomeAdmin(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)
		requestBody := mockUserRequest(1, u.ADMIN)

		selfRest(user).
			PUT("/users_self").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			})
	})
}

func TestDeleteSelf(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)

		selfRest(user).
			DELETE("/users_self").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				user, err := user_repository.FindUserById(user.ID)
				assert.Empty(t, user)
				assert.Error(t, err)
			})
	})
}

func TestFindSelf(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)

		selfRest(user).
			GET("/users_self").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				userResponse := &u.UserResponse{}
				json.Unmarshal(response.Body.Bytes(), userResponse)
				assert.Equal(t, http.StatusOK, response.Code)
				assert.NotEmpty(t, userResponse.ID)
				assert.Equal(t, user.Email, userResponse.Email)
				assert.Equal(t, user.Username, userResponse.Username)
				assert.Equal(t, user.Role, userResponse.Role)
				assert.NotEmpty(t, userResponse.CreatedAt)
				assert.NotEmpty(t, userResponse.UpdatedAt)
				assert.Empty(t, userResponse.DeletedAt)
			})
	})
}

func TestBuyGame(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)
		game1 := mockGame(1)
		game_repository.CreateGame(game1)
		selfRest(user).
			PUT(fmt.Sprintf("/users_self/buy_game/%s", game1.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				games, _ := game_repository.ListUserGames(user.ID)
				assert.Equal(t, 1, len(games))
			})
	})
}

func TestBuyGameDoesntExist(t *testing.T) {
	RunTest(func() {
		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)
		selfRest(user).
			PUT("/users_self/buy_game/aaaaa").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
				games, _ := game_repository.ListUserGames(user.ID)
				assert.Equal(t, 0, len(games))
			})
	})
}
