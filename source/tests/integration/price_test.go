package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/price"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/price_repository"
	"oppaiman-backend-test/source/services/price_service"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockPrice(num int) *price.Price {
	return &price.Price{
		Region: fmt.Sprintf("Region%d", num),
		Value:  float64(num),
	}
}
func mockPriceRequest(num int) *price.PriceRequest {
	return &price.PriceRequest{
		Region: fmt.Sprintf("Region%d", num),
		Value:  float64(num),
	}
}

// TODO:extract paths for better test handling and ease of use
func TestPricesPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/prices_dev").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/prices_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/prices_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindPriceById(t *testing.T) {
	RunTest(func() {
		obj := mockPrice(1)
		price_repository.CreatePrice(obj)

		emptyRest().
			GET(fmt.Sprintf("/prices/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &price.Price{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Region, gameResponse.Region)
				assert.Equal(t, obj.Value, gameResponse.Value)
			})
	})
}

func TestFindPriceByIdNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/prices/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestPostPriceWithExistingRegion(t *testing.T) {
	RunTest(func() {
		obj := mockPrice(1)
		price_repository.CreatePrice(obj)

		requestBody := mockPriceRequest(1)

		baseRest(u.DEVELOPER).
			POST("/prices_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(price_service.ERR_PRICE_DUPLICATE_REGION, obj.Region), response.Body.String())
			})
	})
}

func TestFindAllPrices(t *testing.T) {
	RunTest(func() {
		obj1 := mockPrice(1)
		obj2 := mockPrice(2)
		price_repository.CreatePrice(obj1)
		price_repository.CreatePrice(obj2)

		emptyRest().
			GET("/prices").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []price.Price{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, obj1.Region, gameResponse[0].Region)
				assert.Equal(t, obj1.Value, gameResponse[0].Value)

				assert.Equal(t, obj2.Region, gameResponse[1].Region)
				assert.Equal(t, obj2.Value, gameResponse[1].Value)
			})
	})
}

func TestFindAllPricesEmpty(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/prices").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestUpdatePrice(t *testing.T) {
	RunTest(func() {
		obj := mockPrice(1)
		price_repository.CreatePrice(obj)

		requestBody := mockPriceRequest(1)

		baseRest(u.DEVELOPER).
			PUT(fmt.Sprintf("/prices_dev/%d", obj.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				priceAfterUpdate, _ := price_repository.FindPriceById(obj.ID)

				assert.Equal(t, obj.ID, priceAfterUpdate.ID)
				assert.Equal(t, obj.Region, priceAfterUpdate.Region)
				assert.Equal(t, obj.Value, priceAfterUpdate.Value)
			})
	})
}

func TestUpdatePriceNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockPriceRequest(1)

		baseRest(u.DEVELOPER).
			PUT("/prices/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeletePrice(t *testing.T) {
	RunTest(func() {
		obj := mockPrice(1)
		price_repository.CreatePrice(obj)

		baseRest(u.DEVELOPER).
			DELETE(fmt.Sprintf("/prices_dev/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				game, err := price_repository.FindPriceById(obj.ID)
				assert.Empty(t, game)
				assert.Error(t, err)
			})
	})
}

func TestDeletePriceNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.DEVELOPER).
			DELETE("/prices/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
