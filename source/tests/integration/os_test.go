package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/game_os"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/os_repository"
	"oppaiman-backend-test/source/services/os_service"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockOperatingSystem(num int) *game_os.OperatingSystem {
	return &game_os.OperatingSystem{
		Name:        fmt.Sprintf("Name%d", num),
		Description: fmt.Sprintf("Description%d", num),
	}
}
func mockOperatingSystemRequest(num int) *game_os.OperatingSystemRequest {
	return &game_os.OperatingSystemRequest{
		ID:          uint(num),
		Name:        fmt.Sprintf("Name%d", num),
		Description: fmt.Sprintf("Description%d", num),
	}
}

// TODO:extract paths for better test handling and ease of use
func TestOperatingSystemsPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/oses_dev").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/oses_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/oses_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
	})
}

func TestFindOperatingSystemById(t *testing.T) {
	RunTest(func() {
		obj := mockOperatingSystem(1)
		os_repository.CreateOperatingSystem(obj)

		emptyRest().
			GET(fmt.Sprintf("/oses/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &game_os.OperatingSystem{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Name, gameResponse.Name)
				assert.Equal(t, obj.Description, gameResponse.Description)
			})
	})
}

func TestFindOperatingSystemByIdNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/oses/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindAllOperatingSystems(t *testing.T) {
	RunTest(func() {
		obj1 := mockOperatingSystem(1)
		obj2 := mockOperatingSystem(2)
		os_repository.CreateOperatingSystem(obj1)
		os_repository.CreateOperatingSystem(obj2)

		emptyRest().
			GET("/oses").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []game_os.OperatingSystem{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, obj1.Name, gameResponse[0].Name)
				assert.Equal(t, obj1.Description, gameResponse[0].Description)

				assert.Equal(t, obj2.Name, gameResponse[1].Name)
				assert.Equal(t, obj2.Description, gameResponse[1].Description)
			})
	})
}

func TestFindAllOperatingSystemsEmpty(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/oses").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostOperatingSystem(t *testing.T) {
	RunTest(func() {
		requestBody := mockOperatingSystemRequest(1)

		baseRest(u.DEVELOPER).
			POST("/oses_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, os_repository.ExistsOperatingSystemByName(requestBody.Name))
			})
	})
}

func TestPostOperatingSystemWithExistingTitle(t *testing.T) {
	RunTest(func() {
		obj := mockOperatingSystem(1)
		os_repository.CreateOperatingSystem(obj)

		requestBody := mockOperatingSystemRequest(1)

		baseRest(u.DEVELOPER).
			POST("/oses_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(os_service.ERR_OperatingSystem_DUPLICATE_NAME, obj.Name), response.Body.String())
			})
	})
}

func TestUpdateOperatingSystem(t *testing.T) {
	RunTest(func() {
		obj := mockOperatingSystem(1)
		os_repository.CreateOperatingSystem(obj)

		requestBody := mockOperatingSystemRequest(1)

		baseRest(u.DEVELOPER).
			PUT(fmt.Sprintf("/oses_dev/%d", obj.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				osAfterUpdate, _ := os_repository.FindOperatingSystemById(obj.ID)

				assert.Equal(t, obj.ID, osAfterUpdate.ID)
				assert.Equal(t, obj.Name, osAfterUpdate.Name)
				assert.Equal(t, obj.Description, osAfterUpdate.Description)
			})
	})
}

func TestUpdateOperatingSystemNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockOperatingSystemRequest(1)

		baseRest(u.DEVELOPER).
			PUT("/oses/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeleteOperatingSystem(t *testing.T) {
	RunTest(func() {
		obj := mockOperatingSystem(1)
		os_repository.CreateOperatingSystem(obj)

		baseRest(u.DEVELOPER).
			DELETE(fmt.Sprintf("/oses_dev/%d", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				game, err := os_repository.FindOperatingSystemById(obj.ID)
				assert.Empty(t, game)
				assert.Error(t, err)
			})
	})
}

func TestDeleteOperatingSystemNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.DEVELOPER).
			DELETE("/oses/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
