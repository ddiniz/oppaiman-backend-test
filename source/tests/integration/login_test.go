package tests_integration

import (
	"net/http"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/repository/user_repository"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func mockLoginRequest(username, password string) *u.LoginRequest {
	return &u.LoginRequest{
		UsernameOrEmail: username,
		Password:        password,
	}
}

func TestLoginOK(t *testing.T) {
	RunTest(func() {
		user_repository.CreateUser(mockUser(1, u.ADMIN))

		requestBody := *mockLoginRequest("Username1", "Password1")

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Contains(t, response.Body.String(), `"code":200,"expire"`)
			})
	})
}

func TestLoginInvalidFields(t *testing.T) {
	RunTest(func() {
		requestBody := u.LoginRequest{
			UsernameOrEmail: "Username1",
		}

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, `Key: 'LoginRequest.Password' Error:Field validation for 'Password' failed on the 'required' tag`, response.Body.String())
			})
	})
}

func TestLoginUsernameNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := *mockLoginRequest("admin", "admin")

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusUnauthorized, response.Code)
			})
	})
}

func TestLoginInvalidPassword(t *testing.T) {
	RunTest(func() {
		user_repository.CreateUser(mockUser(1, u.ADMIN))

		requestBody := *mockLoginRequest("admin", "wrongpassword")

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusUnauthorized, response.Code)
				assert.Equal(t, `{"status_code":401,"message":"incorrect Username or Password"}`, response.Body.String())
			})
	})
}
