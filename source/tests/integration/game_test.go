package tests_integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"oppaiman-backend-test/source/domain/game"
	"oppaiman-backend-test/source/domain/pagination"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/domain/usergame"
	"oppaiman-backend-test/source/repository/game_repository"
	"oppaiman-backend-test/source/repository/genre_repository"
	"oppaiman-backend-test/source/repository/user_repository"
	"oppaiman-backend-test/source/services/game_service"
	"testing"
	"time"

	"github.com/appleboy/gofight/v2"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func mockGame(num int) *game.Game {
	return &game.Game{
		Description:      fmt.Sprintf("Description%d", num),
		ShortDescription: fmt.Sprintf("ShortDescription%d", num),
		Title:            fmt.Sprintf("Title%d", num),
		PageUrl:          fmt.Sprintf("PageUrl%d", num),
		Status:           fmt.Sprintf("Status%d", num),
		Prices:           nil,
		Builds:           nil,
		Images:           nil,
		Genres:           nil,
		OperatingSystems: nil,
	}
}

func mockGameRequest(num int) *game.GameRequest {
	return &game.GameRequest{
		Description:      fmt.Sprintf("Description%d", num),
		ShortDescription: fmt.Sprintf("ShortDescription%d", num),
		Title:            fmt.Sprintf("Title%d", num),
		PageUrl:          fmt.Sprintf("PageUrl%d", num),
		Status:           fmt.Sprintf("Status%d", num),
		Prices:           nil,
		Builds:           nil,
		Genres:           nil,
		OperatingSystems: nil,
	}
}

// TODO:extract paths for better test handling and ease of use
func TestGamesPermissionGuard(t *testing.T) {
	RunTest(func() {
		roles := []u.Roles{u.NORMAL}
		for _, role := range roles {
			baseRest(role).
				POST("/games_dev").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				PUT("/games_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
			baseRest(role).
				DELETE("/games_dev/1").
				Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
					assert.Equal(t, http.StatusForbidden, response.Code)
				})
		}
		emptyRest().
			GET("/games/user").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusUnauthorized, response.Code)
			})
	})
}

func TestFindGameById(t *testing.T) {
	RunTest(func() {
		obj := mockGame(1)
		obj.ID = uuid.NewString()
		game_repository.CreateGame(obj)

		emptyRest().
			GET(fmt.Sprintf("/games/%s", obj.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &game.Game{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Description, gameResponse.Description)
				assert.Equal(t, obj.ShortDescription, gameResponse.ShortDescription)
				assert.Equal(t, obj.Title, gameResponse.Title)
				assert.Equal(t, obj.PageUrl, gameResponse.PageUrl)
				assert.Equal(t, obj.CatalogImage, gameResponse.CatalogImage)
				assert.Equal(t, obj.HeaderImage, gameResponse.HeaderImage)
			})
	})
}

func TestFindGameByIdNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/games/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindGameByPageUrl(t *testing.T) {
	RunTest(func() {
		obj := mockGame(1)
		obj.ID = uuid.NewString()
		game_repository.CreateGame(obj)

		emptyRest().
			GET(fmt.Sprintf("/games/page/%s", obj.PageUrl)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := &game.Game{}
				json.Unmarshal(response.Body.Bytes(), gameResponse)
				assert.Equal(t, obj.Description, gameResponse.Description)
				assert.Equal(t, obj.ShortDescription, gameResponse.ShortDescription)
				assert.Equal(t, obj.Title, gameResponse.Title)
				assert.Equal(t, obj.PageUrl, gameResponse.PageUrl)
				assert.Equal(t, obj.CatalogImage, gameResponse.CatalogImage)
				assert.Equal(t, obj.HeaderImage, gameResponse.HeaderImage)
			})
	})
}

func TestFindGameByPageUrlNotFound(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/games/pageUrl/pageurl").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindGamesPaginatedDefaultValues(t *testing.T) {
	RunTest(func() {
		filter := game.FilterGameRequest{
			Name:        "",
			Price:       "-1",
			OnSale:      "false",
			ReleaseDate: "0",
			Genre:       "",
			Limit:       "10",
			Page:        "1",
		}
		query := "?name=" + filter.Name + "&price=" + filter.Price + "&onSale=" + filter.OnSale + "&releaseDate=" + filter.ReleaseDate + "&genre=" + filter.Genre + "&limit=" + filter.Limit + "&page=" + filter.Page
		emptyRest().
			GET("/games/paginated"+query).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(0), paginatorResponse.TotalPages)
				assert.Equal(t, 0, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindGamesPaginated(t *testing.T) {
	RunTest(func() {
		genre := mockGenre(1)
		genre_repository.CreateGenre(genre)
		for i := 0; i < 30; i++ {
			price := mockPrice(10)
			obj := mockGame(i)
			obj.ID = uuid.NewString()
			obj.Prices = append(obj.Prices, *price)
			obj.Genres = append(obj.Genres, *genre)
			game_repository.CreateGame(obj)
		}

		filter := game.FilterGameRequest{
			Name:        "",
			Price:       "-1",
			OnSale:      "false",
			ReleaseDate: "0",
			Genre:       "",
			Limit:       "10",
			Page:        "1",
		}
		query := "?name=" + filter.Name + "&price=" + filter.Price + "&onSale=" + filter.OnSale + "&releaseDate=" + filter.ReleaseDate + "&genre=" + filter.Genre + "&limit=" + filter.Limit + "&page=" + filter.Page
		emptyRest().
			GET("/games/paginated"+query).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(3), paginatorResponse.TotalPages)
				assert.Equal(t, 10, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindGamesPaginatedAndFiltered1(t *testing.T) {
	RunTest(func() {
		price := mockPrice(10)
		genre := mockGenre(1)
		genre_repository.CreateGenre(genre)
		for i := 0; i < 30; i++ {
			obj := mockGame(i)
			obj.ID = uuid.NewString()
			if i == 1 {
				obj.Prices = append(obj.Prices, *price)
				obj.Genres = append(obj.Genres, *genre)
			}

			game_repository.CreateGame(obj)
		}

		filter := game.FilterGameRequest{
			Name:        "Title1",
			Price:       "-1",
			OnSale:      "false",
			ReleaseDate: "0",
			Genre:       "",
			Limit:       "10",
			Page:        "1",
		}
		query := "?name=" + filter.Name + "&price=" + filter.Price + "&onSale=" + filter.OnSale + "&releaseDate=" + filter.ReleaseDate + "&genre=" + filter.Genre + "&limit=" + filter.Limit + "&page=" + filter.Page
		emptyRest().
			GET("/games/paginated"+query).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(3), paginatorResponse.TotalPages)
				assert.Equal(t, 1, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindGamesPaginatedAndFiltered2(t *testing.T) {
	RunTest(func() {
		price := mockPrice(10)
		genre := mockGenre(1)
		genre_repository.CreateGenre(genre)
		for i := 0; i < 30; i++ {
			obj := mockGame(i)
			obj.ID = uuid.NewString()
			if i == 1 {
				obj.Prices = append(obj.Prices, *price)
				obj.Genres = append(obj.Genres, *genre)
			}

			game_repository.CreateGame(obj)
		}

		filter := game.FilterGameRequest{
			Name:        "Title1",
			Price:       "10",
			OnSale:      "false",
			ReleaseDate: "0",
			Genre:       "",
			Limit:       "10",
			Page:        "1",
		}
		query := "?name=" + filter.Name + "&price=" + filter.Price + "&onSale=" + filter.OnSale + "&releaseDate=" + filter.ReleaseDate + "&genre=" + filter.Genre + "&limit=" + filter.Limit + "&page=" + filter.Page
		emptyRest().
			GET("/games/paginated"+query).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(3), paginatorResponse.TotalPages)
			})
	})
}

func TestFindGamesPaginatedAndFiltered3(t *testing.T) {
	RunTest(func() {
		price := mockPrice(10)
		genre := mockGenre(1)
		genre_repository.CreateGenre(genre)
		for i := 0; i < 30; i++ {
			obj := mockGame(i)
			obj.ID = uuid.NewString()
			if i == 1 {
				obj.Prices = append(obj.Prices, *price)
				obj.Genres = append(obj.Genres, *genre)
			}

			game_repository.CreateGame(obj)
		}

		filter := game.FilterGameRequest{
			Name:        "Title1",
			Price:       fmt.Sprintf("%f", price.Value),
			OnSale:      "false",
			ReleaseDate: fmt.Sprintf("%d", time.Now().Add(time.Hour).UnixMilli()),
			Genre:       genre.Name,
			Limit:       "10",
			Page:        "1",
		}
		query := "?name=" + filter.Name + "&price=" + filter.Price + "&onSale=" + filter.OnSale + "&releaseDate=" + filter.ReleaseDate + "&genre=" + filter.Genre + "&limit=" + filter.Limit + "&page=" + filter.Page
		emptyRest().
			GET("/games/paginated"+query).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &pagination.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, uint(10), paginatorResponse.Limit)
				assert.Equal(t, uint(1), paginatorResponse.Page)
				assert.Equal(t, uint(3), paginatorResponse.TotalPages)
			})
	})
}

func TestFindAllGames(t *testing.T) {
	RunTest(func() {
		game1 := mockGame(1)
		game1.ID = uuid.NewString()
		game2 := mockGame(2)
		game2.ID = uuid.NewString()
		game_repository.CreateGame(game1)
		game_repository.CreateGame(game2)

		emptyRest().
			GET("/games").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []game.Game{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, game1.Description, gameResponse[0].Description)
				assert.Equal(t, game1.ShortDescription, gameResponse[0].ShortDescription)
				assert.Equal(t, game1.Title, gameResponse[0].Title)
				assert.Equal(t, game1.PageUrl, gameResponse[0].PageUrl)
				assert.Equal(t, game1.CatalogImage, gameResponse[0].CatalogImage)
				assert.Equal(t, game1.HeaderImage, gameResponse[0].HeaderImage)

				assert.Equal(t, game2.Description, gameResponse[1].Description)
				assert.Equal(t, game2.ShortDescription, gameResponse[1].ShortDescription)
				assert.Equal(t, game2.Title, gameResponse[1].Title)
				assert.Equal(t, game2.PageUrl, gameResponse[1].PageUrl)
				assert.Equal(t, game2.CatalogImage, gameResponse[1].CatalogImage)
				assert.Equal(t, game2.HeaderImage, gameResponse[1].HeaderImage)
			})
	})
}

func TestFindAllGamesEmpty(t *testing.T) {
	RunTest(func() {
		emptyRest().
			GET("/games").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestFindAllUserGames(t *testing.T) {
	RunTest(func() {
		game1 := mockGame(1)
		game2 := mockGame(2)
		game3 := mockGame(3)
		game1.ID = uuid.NewString()
		game2.ID = uuid.NewString()
		game3.ID = uuid.NewString()
		game_repository.CreateGame(game1)
		game_repository.CreateGame(game2)
		game_repository.CreateGame(game3)

		user := mockUser(1, u.NORMAL)
		user_repository.CreateUser(user)
		userGame1 := usergame.UserGame{
			UserID: user.ID,
			GameID: game1.ID,
		}
		userGame2 := usergame.UserGame{
			UserID: user.ID,
			GameID: game2.ID,
		}
		user.BoughtGames = append(user.BoughtGames, userGame1)
		user.BoughtGames = append(user.BoughtGames, userGame2)
		user_repository.UpdateUser(user)

		selfRest(user).
			GET("/games/user").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)

				gameResponse := []game.Game{}
				json.Unmarshal(response.Body.Bytes(), &gameResponse)

				assert.Equal(t, 2, len(gameResponse))

				assert.Equal(t, game1.Description, gameResponse[0].Description)
				assert.Equal(t, game1.ShortDescription, gameResponse[0].ShortDescription)
				assert.Equal(t, game1.Title, gameResponse[0].Title)
				assert.Equal(t, game1.PageUrl, gameResponse[0].PageUrl)
				assert.Equal(t, game1.CatalogImage, gameResponse[0].CatalogImage)
				assert.Equal(t, game1.HeaderImage, gameResponse[0].HeaderImage)

				assert.Equal(t, game2.Description, gameResponse[1].Description)
				assert.Equal(t, game2.ShortDescription, gameResponse[1].ShortDescription)
				assert.Equal(t, game2.Title, gameResponse[1].Title)
				assert.Equal(t, game2.PageUrl, gameResponse[1].PageUrl)
				assert.Equal(t, game2.CatalogImage, gameResponse[1].CatalogImage)
				assert.Equal(t, game2.HeaderImage, gameResponse[1].HeaderImage)
			})
	})
}

func TestFindUserGamesEmpty(t *testing.T) {
	RunTest(func() {
		baseRest(u.NORMAL).
			GET("/games/user").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostGame(t *testing.T) {
	RunTest(func() {
		requestBody := mockGameRequest(1)

		baseRest(u.DEVELOPER).
			POST("/games_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, game_repository.ExistsGameByTitle(requestBody.Title))
			})
	})
}

func TestPostGameWithExistingTitle(t *testing.T) {
	RunTest(func() {
		game := mockGame(1)
		game.ID = uuid.NewString()
		game_repository.CreateGame(game)

		requestBody := mockGameRequest(1)
		baseRest(u.DEVELOPER).
			POST("/games_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(game_service.ERR_GAME_DUPLICATE_TITLE, game.Title), response.Body.String())
			})
	})
}
func TestPostGameWithExistingPageUrl(t *testing.T) {
	RunTest(func() {
		game := mockGame(1)
		game.ID = uuid.NewString()
		game_repository.CreateGame(game)

		requestBody := mockGameRequest(1)
		requestBody.Title = "Title2"
		baseRest(u.DEVELOPER).
			POST("/games_dev").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, fmt.Sprintf(game_service.ERR_GAME_DUPLICATE_PAGEURL, game.PageUrl), response.Body.String())
			})
	})
}

func TestUpdateGame(t *testing.T) {
	RunTest(func() {
		game := mockGame(1)
		game.ID = uuid.NewString()
		game_repository.CreateGame(game)

		requestBody := mockGameRequest(1)

		baseRest(u.DEVELOPER).
			PUT(fmt.Sprintf("/games_dev/%s", game.ID)).
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				gameAfterUpdate, _ := game_repository.FindGameById(game.ID)

				assert.Equal(t, game.ID, gameAfterUpdate.ID)
				assert.Equal(t, requestBody.Description, gameAfterUpdate.Description)
				assert.Equal(t, requestBody.ShortDescription, gameAfterUpdate.ShortDescription)
				assert.Equal(t, requestBody.Title, gameAfterUpdate.Title)
				assert.Equal(t, requestBody.PageUrl, gameAfterUpdate.PageUrl)
			})
	})
}

func TestUpdateGameNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := mockGameRequest(1)

		baseRest(u.DEVELOPER).
			PUT("/game/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeleteGame(t *testing.T) {
	RunTest(func() {
		game := mockGame(1)
		game.ID = uuid.NewString()
		game_repository.CreateGame(game)

		baseRest(u.DEVELOPER).
			DELETE(fmt.Sprintf("/games_dev/%s", game.ID)).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				game, err := game_repository.FindGameById(game.ID)
				assert.Empty(t, game)
				assert.Error(t, err)
			})
	})
}

func TestDeleteGameNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(u.DEVELOPER).
			DELETE("/game/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
