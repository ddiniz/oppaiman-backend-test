package tests_integration

import (
	"log"

	"oppaiman-backend-test/source/config"
	u "oppaiman-backend-test/source/domain/user"
	db "oppaiman-backend-test/source/repository/database"
	migrations "oppaiman-backend-test/source/repository/migrations"
	"oppaiman-backend-test/source/rest/routes"
	test_containers "oppaiman-backend-test/source/tests/containers"
	test_utils "oppaiman-backend-test/source/tests/utils"

	"os"
	"testing"
	"time"

	"github.com/appleboy/gofight/v2"
	"github.com/gin-gonic/gin"
)

/*
	This file needs to be the first one on the order inside the package
	otherwise tests can fail because init functions ordering
*/

var RunTest = test_utils.CreateForEach(beforeEach, AfterEach)
var runningContainers = []*test_containers.ContainerResult{}
var router = &gin.Engine{}

var (
	adminToken      = test_utils.GenerateJwtToken("ADMIN", u.ADMIN)
	supervisorToken = test_utils.GenerateJwtToken("DEVELOPER", u.DEVELOPER)
	normalToken     = test_utils.GenerateJwtToken("NORMAL", u.NORMAL)
)

func TestMain(m *testing.M) {
	log.Println("Starting test setup")
	start := time.Now()
	beforeAll()
	log.Printf("Setup took %s seconds\n", time.Since(start))
	exitVal := m.Run()

	for _, container := range runningContainers {
		container.Terminate()
	}

	os.Exit(exitVal)
}

func beforeAll() {
	config.ReadConfigFile()
	//TODO: add a flag that for this case
	// wg := &sync.WaitGroup{}
	// test_utils.RunInParallel(wg, startMySqlContainer)
	// wg.Wait()

	db.InitDB()
	router = gin.New()
	routes.SetupRouterRoutes(router)
}

func startMySqlContainer() {
	mysqlContainer := test_containers.SetupMySQLTestContainer(
		&test_containers.MySQLContainerConfig{
			Database:     config.GetDatabaseName(),
			RootPassword: config.GetDatabasePassword(),
		},
	)
	runningContainers = append(runningContainers, mysqlContainer)
}

func beforeEach() {
	migrations.AutoMigrate()
}

func AfterEach() {
	migrations.DropAll()
}

func baseRest(role u.Roles) *gofight.RequestConfig {
	jwt := jwtFromRole(role)

	return &gofight.RequestConfig{
		Debug: true,
		Headers: gofight.H{
			"Authorization": "Bearer " + jwt,
		}}
}

func selfRest(user *u.User) *gofight.RequestConfig {
	jwt := test_utils.GenerateJwtToken(user.ID, user.Role)

	return &gofight.RequestConfig{
		Debug: true,
		Headers: gofight.H{
			"Authorization": "Bearer " + jwt,
		}}
}

func emptyRest() *gofight.RequestConfig {
	return &gofight.RequestConfig{Debug: true}
}

func jwtFromRole(role u.Roles) string {
	switch role {
	case u.ADMIN:
		return adminToken
	case u.DEVELOPER:
		return supervisorToken
	default:
		return normalToken
	}
}
