package test_utils

import (
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/services/authentication"
	"sync"
)

func CreateForEach(setUp func(), tearDown func()) func(func()) {
	return func(testFunc func()) {
		setUp()
		testFunc()
		tearDown()
	}
}

func GenerateJwtToken(id string, role u.Roles) string {
	token, _, _ := authentication.Jwt.TokenGenerator(
		&authentication.AppClaims{
			ID:   id,
			Role: role,
		},
	)

	return token
}

func RunInParallel(wg *sync.WaitGroup, work func()) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		work()
	}()
}
