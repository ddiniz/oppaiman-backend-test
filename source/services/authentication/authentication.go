package authentication

import (
	"net/http"
	"oppaiman-backend-test/source/domain/user"

	"github.com/gin-gonic/gin"
)

func LoginGuard() gin.HandlerFunc {
	return Jwt.MiddlewareFunc()
}

func RoleGuard(roles []user.Roles) gin.HandlerFunc {
	return func(c *gin.Context) {
		claims := ExtractClaims(c)

		for i := 0; i < len(roles); i++ {
			if claims.Role == roles[i] {
				return
			}
		}

		c.AbortWithStatus(http.StatusForbidden)
	}
}
