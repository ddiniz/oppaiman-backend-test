package authentication

import (
	"log"
	"oppaiman-backend-test/source/config"
	"oppaiman-backend-test/source/domain/user"
	"time"

	rp "oppaiman-backend-test/source/repository/user_repository"
	rest_utils "oppaiman-backend-test/source/rest/utils"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// TODO: solve the import loop that would happen if the login endpoint was declared in authentication_rest.go
// Login godoc
// @Summary     Login
// @Description Login and generate jwt auth
// @Tags        Auth
// @Accept      json
// @Produce     json
// @Param       auth body     requests.Auth true "Auth Info"
// @Success     200  {object} middleware.LoginOK
// @Failure     400  {object} exception.HttpException
// @Failure     401  {object} middleware.LoginError
// @Router      /login [post]
func loginHandler(c *gin.Context) (interface{}, error) {
	auth := &user.LoginRequest{}
	rest_utils.ReadBody(c, auth)

	user, err := rp.FindUserByUsername(auth.UsernameOrEmail)

	if err != nil {
		return nil, jwt.ErrFailedAuthentication
	}

	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(auth.Password)); err != nil {
		return nil, jwt.ErrFailedAuthentication
	}

	return &AppClaims{
		ID:       user.ID,
		Role:     user.Role,
		Username: user.Username,
	}, nil
}

type LoginOK struct {
	Code   int    `json:"code" example:"200"`
	Token  string `json:"token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"`
	Expire string `json:"expire" example:"2006-01-02T15:04:05Z07:00"`
}

type LoginError struct {
	StatusCode int    `json:"status_code" example:"401"`
	Message    string `json:"message" example:"Invalid username or password"`
}

type AppClaims struct {
	ID       string
	Role     user.Roles
	Username string
}

var Jwt = jwtMiddleware()
var identityKey = config.GetGetJwtIdentityKey()

func jwtMiddleware() *jwt.GinJWTMiddleware {

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:           config.GetJwtRealm(),
		Key:             config.GetJwtSecretKey(),
		Timeout:         config.GetJwtTimeout(),
		MaxRefresh:      config.GetJwtMaxRefresh(),
		IdentityKey:     identityKey,
		Authenticator:   loginHandler,
		PayloadFunc:     payloadHandler,
		IdentityHandler: identityHandler,
		Authorizator:    autorizatorHandler,
		Unauthorized:    unauthorizedHandler,
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Fatal("JWT Initialization Error: " + err.Error())
	}

	return authMiddleware
}

func payloadHandler(data interface{}) jwt.MapClaims {
	user := data.(*AppClaims)

	return jwt.MapClaims{
		identityKey: user.ID,
		"id":        user.ID,
		"role":      int(user.Role),
		"username":  user.Username,
	}
}

func identityHandler(c *gin.Context) interface{} {
	return ExtractClaims(c)
}

func autorizatorHandler(data interface{}, c *gin.Context) bool {
	return true
}

func unauthorizedHandler(c *gin.Context, code int, message string) {
	c.JSON(code, LoginError{
		StatusCode: code,
		Message:    message,
	})
}

func ExtractClaims(c *gin.Context) *AppClaims {
	claims := jwt.ExtractClaims(c)
	return &AppClaims{
		ID:       claims[identityKey].(string),
		Role:     user.Roles(claims["role"].(float64)),
		Username: claims["username"].(string),
	}
}
