package image_service

import (
	"errors"
	s "oppaiman-backend-test/source/domain/image"
	db "oppaiman-backend-test/source/repository/database"
	rp "oppaiman-backend-test/source/repository/image_repository"
	exceptions "oppaiman-backend-test/source/rest/exceptions"

	"gorm.io/gorm"
)

var ERR_FIND_IMAGE = "Error while trying to find image with error: %s"
var ERR_DELETE_IMAGE = "Error ocurred while trying to delete image %d with error: %s"
var ERR_UPDATE_IMAGE = "Error ocurred while trying to update image %d with error: %s"
var ERR_CREATE_IMAGE = "Error while trying to create new image with error: %s"
var ERR_IMAGE_NOT_FOUND = "Image with id {%d} was not found"
var ERR_IMAGE_DUPLICATE_NAME = "Image with name {%s} already exists"

func ListImages() []s.ImageResponse {
	image, err := rp.ListImages()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_IMAGE, err)

	}

	imageResponse := []s.ImageResponse{}
	for _, image := range image {
		imageResponse = append(imageResponse, *s.MapToImageResponse(&image))
	}
	return imageResponse
}

func FindImageById(id string) *s.ImageResponse {
	image, err := rp.FindImageById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_IMAGE_NOT_FOUND, id)
	}

	return s.MapToImageResponse(image)
}

func CreateImage(req *s.ImageRequest) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		nameExists := rp.ExistsImageByName(req.Name, tx)

		if nameExists {
			return exceptions.BadRequestException(ERR_IMAGE_DUPLICATE_NAME, req.Name)
		}

		image := &s.Image{
			Name: req.Name,
			Src:  req.Src,
		}

		if err := rp.CreateImage(image, tx); err != nil {
			return exceptions.InternalServerException(ERR_CREATE_IMAGE, err)
		}

		return nil
	})
}

func UpdateImage(req *s.ImageRequest, id string) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		image, err := rp.FindImageById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_IMAGE_NOT_FOUND, id)
		}

		image.Src = req.Src
		image.Name = req.Name

		if err := rp.UpdateImage(image, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_IMAGE, id, err)
		}

		return nil
	})
}

func DeleteImage(id string) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeleteImage(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_IMAGE_NOT_FOUND, id)
			}

			return exceptions.InternalServerException(ERR_DELETE_IMAGE, id, err)
		}

		return nil
	})
}
