package genre_service

import (
	"errors"
	g "oppaiman-backend-test/source/domain/genre"
	db "oppaiman-backend-test/source/repository/database"
	rp "oppaiman-backend-test/source/repository/genre_repository"
	exceptions "oppaiman-backend-test/source/rest/exceptions"

	"gorm.io/gorm"
)

var ERR_FIND_GENRE = "Error while trying to find genre with error: %s"
var ERR_DELETE_GENRE = "Error ocurred while trying to delete genre %d with error: %s"
var ERR_UPDATE_GENRE = "Error ocurred while trying to update genre %d with error: %s"
var ERR_CREATE_GENRE = "Error while trying to create new genre with error: %s"
var ERR_GENRE_NOT_FOUND = "Genre with id {%d} was not found"
var ERR_GENRE_DUPLICATE_NAME = "Genre with name {%s} already exists"

func ListGenres() []*g.GenreResponse {
	genre, err := rp.ListGenres()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_GENRE, err)

	}

	genreResponse := []*g.GenreResponse{}
	for _, genre := range genre {
		genreResponse = append(genreResponse, g.MapToGenreResponse(&genre))
	}
	return genreResponse
}

func FindGenreById(id uint) *g.GenreResponse {
	genre, err := rp.FindGenreById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_GENRE_NOT_FOUND, id)
	}

	return g.MapToGenreResponse(genre)
}

func CreateGenre(req *g.GenreRequest) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		nameExists := rp.ExistsGenreByName(req.Name, tx)

		if nameExists {
			return exceptions.BadRequestException(ERR_GENRE_DUPLICATE_NAME, req.Name)
		}

		genre := &g.Genre{
			Description: req.Description,
			Name:        req.Name,
		}

		if err := rp.CreateGenre(genre, tx); err != nil {
			return exceptions.InternalServerException(ERR_CREATE_GENRE, err)
		}

		return nil
	})
}
func FindOrCreate(req *g.GenreRequest) *g.Genre {
	genre, err := rp.FindGenreByName(req.Name)
	if err != nil {
		db.UsingTransactional(func(tx *db.TransactionalOperation) error {
			nameExists := rp.ExistsGenreByName(req.Name, tx)

			if nameExists {
				return nil
			}

			genre := &g.Genre{
				Description: req.Description,
				Name:        req.Name,
			}

			if err := rp.CreateGenre(genre, tx); err != nil {
				return exceptions.InternalServerException(ERR_CREATE_GENRE, err)
			}

			return nil
		})
		genre, err = rp.FindGenreByName(req.Name)
		if err != nil {
			exceptions.ThrowNotFoundException(ERR_GENRE_NOT_FOUND, req.ID)
		}
	}
	return genre
}

func UpdateGenre(req *g.GenreRequest, id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		genre, err := rp.FindGenreById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_GENRE_NOT_FOUND, id)
		}

		genre.Description = req.Description
		genre.Name = req.Name

		if err := rp.UpdateGenre(genre, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_GENRE, id, err)
		}

		return nil
	})
}
func DeleteGenre(id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeleteGenre(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_GENRE_NOT_FOUND, id)
			}

			return exceptions.InternalServerException(ERR_DELETE_GENRE, id, err)
		}

		return nil
	})
}
