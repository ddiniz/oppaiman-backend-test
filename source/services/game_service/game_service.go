package game_service

import (
	"errors"
	"mime/multipart"
	"oppaiman-backend-test/source/domain/build"
	g "oppaiman-backend-test/source/domain/game"
	"oppaiman-backend-test/source/domain/game_os"
	"oppaiman-backend-test/source/domain/genre"
	"oppaiman-backend-test/source/domain/image"
	p "oppaiman-backend-test/source/domain/pagination"
	"oppaiman-backend-test/source/domain/price"
	db "oppaiman-backend-test/source/repository/database"
	file_repository "oppaiman-backend-test/source/repository/file_repository"
	rp "oppaiman-backend-test/source/repository/game_repository"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	"oppaiman-backend-test/source/services/genre_service"
	"oppaiman-backend-test/source/services/os_service"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

var ERR_FIND_GAME = "Error while trying to find games with error: %s"
var ERR_FIND_GAME_PAGINATED = "Error while trying to find games paginated with error: %s"
var ERR_DELETE_GAME = "Error ocurred while trying to delete game {%d} with error: %s"
var ERR_UPDATE_GAME = "Error ocurred while trying to update game {%d} with error: %s"
var ERR_CREATE_GAME = "Error while trying to create new game with error: %s"
var ERR_GAME_NOT_FOUND = "Game with id {%d} was not found"
var ERR_GAME_PAGEURL_NOT_FOUND = "Game with pageUrl {%s} was not found"
var ERR_GAME_DUPLICATE_PAGEURL = "Game with pageUrl {%s} already exists"
var ERR_GAME_DUPLICATE_TITLE = "Game with title {%s} already exists"
var ERR_GAME_FILE_UPLOAD = "Error when trying to save file : {%s}"

func ListGames() []*g.GameResponse {
	games, err := rp.ListGames()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_GAME, err)
	}

	gamesResponse := []*g.GameResponse{}
	for _, game := range games {
		gamesResponse = append(gamesResponse, g.MapToGameResponse(&game))
	}
	return gamesResponse
}
func ListUserGames(userID string) []*g.GameResponse {
	games, err := rp.ListUserGames(userID)

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_GAME, err)
	}

	gamesResponse := []*g.GameResponse{}
	for _, game := range games {
		gamesResponse = append(gamesResponse, g.MapToGameResponse(&game))
	}
	return gamesResponse
}

func FindGamesPaginated(filter g.FilterGame) *p.Pagination {
	paginated, err := rp.FindGamesPaginated(filter)
	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_GAME_PAGINATED, err)
	}

	gamesResponse := []*g.GameResponse{}
	for _, game := range paginated.Rows.([]g.Game) {
		gamesResponse = append(gamesResponse, g.MapToGameResponse(&game))
	}
	paginated.Rows = gamesResponse

	return paginated
}

func FindGameById(id string) *g.GameResponse {
	game, err := rp.FindGameById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_GAME_NOT_FOUND, id)
	}

	return g.MapToGameResponse(game)
}
func FindGameByPageUrl(pageUrl string) *g.GameResponse {
	game, err := rp.FindGameByPageUrl(pageUrl)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_GAME_PAGEURL_NOT_FOUND, pageUrl)
	}

	return g.MapToGameResponse(game)
}

func preparePrices(req []price.PriceRequest) []price.Price {
	prices := make([]price.Price, len(req))
	for i, reqPrice := range req {
		prices[i] = price.Price{
			Region: reqPrice.Region,
			Value:  reqPrice.Value,
		}
	}
	return prices
}

func prepareBuilds(
	req []build.BuildRequest,
	context *gin.Context,
	gameFile *multipart.FileHeader,
	folderPath string,
) ([]build.Build, error) {
	gamePath := folderPath + "/" + gameFile.Filename
	builds := make([]build.Build, len(req))
	for i, reqBuild := range req {
		builds[i] = build.Build{
			Name:        reqBuild.Name,
			Description: reqBuild.Description,
			Version:     reqBuild.Version,
			File:        gamePath,
		}
	}
	if err := context.SaveUploadedFile(gameFile, gamePath); err != nil {
		return nil, exceptions.BadRequestException(ERR_GAME_FILE_UPLOAD, err.Error())
	}
	return builds, nil
}

func saveGameFile(context *gin.Context, imgFileHeader *multipart.FileHeader, folderPath string) {

}

func saveSingleImageFile(context *gin.Context, imgFileHeader *multipart.FileHeader, folderPath string) (*image.Image, error) {
	fileId := uuid.NewString()
	filePath := folderPath + "/" + fileId
	imgObj := image.Image{
		ID:   fileId,
		Name: imgFileHeader.Filename,
		Src:  filePath,
	}
	if err := context.SaveUploadedFile(imgFileHeader, filePath); err != nil {
		return nil, exceptions.BadRequestException(ERR_GAME_FILE_UPLOAD, err.Error())
	}
	return &imgObj, nil
}

func saveMultipleImageFiles(context *gin.Context, multipleImageFileHeaders []*multipart.FileHeader, folderPath string) ([]image.Image, error) {
	images := make([]image.Image, 0)
	for _, img := range multipleImageFileHeaders {
		imgObj, err := saveSingleImageFile(context, img, folderPath)
		if err != nil {
			return nil, exceptions.BadRequestException(ERR_GAME_FILE_UPLOAD, err.Error())
		}
		images = append(images, *imgObj)
	}
	return images, nil
}

func CreateGame(
	context *gin.Context,
	req *g.GameRequest,
	gameFile *multipart.FileHeader,
	catalogImage *multipart.FileHeader,
	headerImage *multipart.FileHeader,
	screenshots []*multipart.FileHeader,
) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		titleExists := rp.ExistsGameByTitle(req.Title)

		if titleExists {
			return exceptions.BadRequestException(ERR_GAME_DUPLICATE_TITLE, req.Title)
		}
		pageUrl := strings.Replace(req.PageUrl, " ", "", -1)
		pageUrl = strings.Replace(pageUrl, "\t", "", -1)
		pageUrlExists := rp.ExistsGameByPageUrl(pageUrl)
		if pageUrlExists {
			return exceptions.BadRequestException(ERR_GAME_DUPLICATE_PAGEURL, req.PageUrl)
		}

		prices := preparePrices(req.Prices)

		gameID := uuid.NewString()
		folderPath, err := file_repository.GetOrCreatePublicFolder(gameID)
		if err != nil {
			return err
		}
		protectedFolderPath, err := file_repository.GetOrCreateProtectedFolder(gameID)
		if err != nil {
			return err
		}

		builds, err := prepareBuilds(req.Builds, context, gameFile, protectedFolderPath)
		if err != nil {
			defer file_repository.DeleteFolder(folderPath)
			return err
		}

		catalogImageObj, err := saveSingleImageFile(context, catalogImage, folderPath)
		if err != nil {
			defer file_repository.DeleteFolder(folderPath)
			return err
		}

		headerImageObj, err := saveSingleImageFile(context, catalogImage, folderPath)
		if err != nil {
			defer file_repository.DeleteFolder(folderPath)
			return err
		}

		screenshotObjs, err := saveMultipleImageFiles(context, screenshots, folderPath)
		if err != nil {
			defer file_repository.DeleteFolder(folderPath)
			return err
		}

		game := &g.Game{
			ID:               gameID,
			Description:      req.Description,
			ShortDescription: req.ShortDescription,
			Title:            req.Title,
			PageUrl:          pageUrl,
			Prices:           prices,
			Builds:           builds,
			Images:           screenshotObjs,
			CatalogImage:     *catalogImageObj,
			HeaderImage:      *headerImageObj,
		}
		genres := make([]genre.Genre, len(req.Genres))
		for i, reqGen := range req.Genres {
			genres[i] = *genre_service.FindOrCreate(&reqGen)
		}
		game.Genres = genres

		oses := make([]game_os.OperatingSystem, len(req.OperatingSystems))
		for i, reqOperatingSystem := range req.OperatingSystems {
			oses[i] = *os_service.FindOrCreate(&reqOperatingSystem)
		}
		game.OperatingSystems = oses

		if err := rp.CreateGame(game, tx); err != nil {
			defer file_repository.DeleteFolder(folderPath)
			return exceptions.InternalServerException(ERR_CREATE_GAME, err)
		}

		return nil
	})
}
func UpdateGame(req *g.GameRequest, id string) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		game, err := rp.FindGameById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_GAME_NOT_FOUND, id)
		}

		game.Description = req.Description
		game.ShortDescription = req.ShortDescription
		game.Title = req.Title
		game.PageUrl = req.PageUrl
		// game.Prices = req.Prices
		// game.Builds = req.Builds
		// game.Images = req.Images
		// game.Genres = req.Genres
		// game.OperatingSystems = req.OperatingSystems

		if err := rp.UpdateGame(game, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_GAME, id, err)
		}
		return nil
	})
}
func DeleteGame(id string) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeleteGame(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_GAME_NOT_FOUND, id)
			}
			return exceptions.InternalServerException(ERR_DELETE_GAME, id, err)
		}
		return nil
	})
}
