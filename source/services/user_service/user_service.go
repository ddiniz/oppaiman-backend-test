package user_service

import (
	"errors"
	p "oppaiman-backend-test/source/domain/pagination"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/domain/usergame"
	db "oppaiman-backend-test/source/repository/database"
	"oppaiman-backend-test/source/repository/file_repository"
	"oppaiman-backend-test/source/repository/game_repository"
	rp "oppaiman-backend-test/source/repository/user_repository"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	"oppaiman-backend-test/source/services/authentication"
	"oppaiman-backend-test/source/services/game_service"
	"os"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var ERR_FIND_USER = "Error while trying to find users with error: %s"
var ERR_FIND_USER_PAGINATED = "Error while trying to find users paginated with error: %s"
var ERR_DELETE_USER = "Error ocurred while trying to delete user {%d} with error: %s"
var ERR_UPDATE_USER = "Error ocurred while trying to update user {%d} with error: %s"
var ERR_CREATE_USER = "Error while trying to create new user with error: %s"
var ERR_USER_NOT_FOUND = "User with id {%d} was not found"
var ERR_USER_EMAIL_NOT_FOUND = "User with email {%s} was not found"
var ERR_USER_USERNAME_NOT_FOUND = "User with username {%s} was not found"
var ERR_USER_DUPLICATE_USERNAME = "User with username {%s} already exists"
var ERR_USER_DUPLICATE_EMAIL = "User with email {%s} already exists"
var ERR_TRYING_TO_BE_ADMIN = "Invalid operation"
var ERR_USER_ALREADY_HAS_GAME = "User has already bought this game."
var ERR_USER_DOES_NOT_OWN_GAME = "User does not own this game."

func ListUsers() []*u.UserResponse {
	users, err := rp.ListUsers()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_USER, err)
	}

	usersResponse := []*u.UserResponse{}
	for _, user := range users {
		usersResponse = append(usersResponse, u.MapToUserResponse(&user))
	}
	return usersResponse
}
func FindUsersPaginated(limit uint, page uint) *p.Pagination {
	paginated, err := rp.FindUsersPaginated(limit, page)
	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_USER_PAGINATED, err)
	}

	usersResponse := []*u.UserResponse{}
	for _, user := range paginated.Rows.([]u.User) {
		usersResponse = append(usersResponse, u.MapToUserResponse(&user))
	}
	paginated.Rows = usersResponse

	return paginated
}
func FindUserById(id string) *u.UserResponse {
	user, err := rp.FindUserById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_USER_NOT_FOUND, id)
	}
	return u.MapToUserResponse(user)
}
func FindUserByUsername(username string) *u.UserResponse {
	user, err := rp.FindUserByUsername(username)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_USER_USERNAME_NOT_FOUND, username)
	}
	return u.MapToUserResponse(user)
}
func FindUserByEmail(email string) *u.UserResponse {
	user, err := rp.FindUserByEmail(email)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_USER_EMAIL_NOT_FOUND, email)
	}
	return u.MapToUserResponse(user)
}

func CreateUser(req *u.UserRequest) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		existsUsername := rp.ExistsUserByUsername(req.Username)
		if existsUsername {
			return exceptions.BadRequestException(ERR_USER_DUPLICATE_USERNAME, req.Username)
		}
		existsEmail := rp.ExistsUserByEmail(req.Email)
		if existsEmail {
			return exceptions.BadRequestException(ERR_USER_DUPLICATE_EMAIL, req.Email)
		}
		if req.Role == u.ADMIN {
			return exceptions.BadRequestException(ERR_TRYING_TO_BE_ADMIN)
		}

		user := &u.User{
			Username: req.Username,
			Email:    req.Email,
			Password: req.Password,
			Role:     req.Role,
		}

		if err := rp.CreateUser(user, tx); err != nil {
			return exceptions.InternalServerException(ERR_CREATE_USER, err)
		}
		return nil
	})
}
func UpdateUser(req *u.UserRequest, id string) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		user, err := rp.FindUserById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_USER_NOT_FOUND, id)
		}

		user.Username = req.Username
		user.Email = req.Email
		user.Password = req.Password
		user.Role = req.Role

		if err := rp.UpdateUser(user, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_USER, id, err)
		}
		return nil
	})
}
func DeleteUser(id string) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeleteUser(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_USER_NOT_FOUND, id)
			}
			return exceptions.InternalServerException(ERR_DELETE_USER, id, err)
		}
		return nil
	})
}

func DeleteSelf(c *gin.Context) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		claims := authentication.ExtractClaims(c)
		if err := rp.DeleteUser(claims.ID, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_USER_NOT_FOUND, claims.ID)
			}
			return exceptions.InternalServerException(ERR_DELETE_USER, claims.ID, err)
		}
		return nil
	})
}
func UpdateSelf(req *u.UserRequest, c *gin.Context) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		claims := authentication.ExtractClaims(c)
		user, err := rp.FindUserById(claims.ID)
		if err != nil {
			return exceptions.NotFoundException(ERR_USER_NOT_FOUND, claims.ID)
		}

		if req.Role == u.ADMIN {
			return exceptions.BadRequestException(ERR_TRYING_TO_BE_ADMIN)
		}

		user.Username = req.Username
		user.Email = req.Email
		user.Password = req.Password
		user.Role = req.Role

		if err := rp.UpdateUser(user, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_USER, claims.ID, err)
		}
		return nil
	})
}

func FindSelf(c *gin.Context) *u.UserResponse {
	claims := authentication.ExtractClaims(c)
	user, err := rp.FindUserById(claims.ID)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_USER_NOT_FOUND, claims.ID)
	}
	return u.MapToUserResponse(user)
}

func BuyGame(gameId string, c *gin.Context) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		claims := authentication.ExtractClaims(c)
		user, err := rp.FindUserById(claims.ID)
		if err != nil {
			return exceptions.NotFoundException(ERR_USER_NOT_FOUND, claims.ID)
		}

		game, err := game_repository.FindGameById(gameId)
		if err != nil {
			return exceptions.NotFoundException(game_service.ERR_GAME_NOT_FOUND, claims.ID)
		}
		hasGame := rp.UserHasGame(user.ID, game.ID)
		if hasGame {
			return exceptions.NotFoundException(ERR_USER_ALREADY_HAS_GAME, claims.ID)
		}

		userGame := usergame.UserGame{
			UserID: user.ID,
			GameID: game.ID,
		}

		user.BoughtGames = append(user.BoughtGames, userGame)

		if err := rp.UpdateUser(user, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_USER, claims.ID, err)
		}
		return nil
	})
}

func HasGame(pageUrl string, c *gin.Context) bool {
	claims := authentication.ExtractClaims(c)
	user, err := rp.FindUserById(claims.ID)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_USER_NOT_FOUND, claims.ID)
	}

	game, err := game_repository.FindGameByPageUrl(pageUrl)
	if err != nil {
		exceptions.ThrowNotFoundException(game_service.ERR_GAME_NOT_FOUND)
	}
	return rp.UserHasGame(user.ID, game.ID)
}

func GetGame(gameId string, buildId string, c *gin.Context) (*os.File, string, string) {
	claims := authentication.ExtractClaims(c)
	user, err := rp.FindUserById(claims.ID)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_USER_NOT_FOUND, claims.ID)
	}

	game, err := game_repository.FindGameByIdWithBuilds(gameId)
	if err != nil {
		exceptions.ThrowNotFoundException(game_service.ERR_GAME_NOT_FOUND, claims.ID)
	}
	hasGame := rp.UserHasGame(user.ID, game.ID)
	if !hasGame {
		exceptions.ThrowNotFoundException(ERR_USER_DOES_NOT_OWN_GAME)
	}

	build := game.Builds[0]
	filePath := build.File
	gameFile, err := file_repository.GetProtectedFile(filePath)
	if err != nil {
		exceptions.ThrowBadRequestException("")
	}
	return gameFile, build.Name, filePath
}
