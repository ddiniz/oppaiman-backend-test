package price_service

import (
	"errors"
	p "oppaiman-backend-test/source/domain/price"
	db "oppaiman-backend-test/source/repository/database"
	rp "oppaiman-backend-test/source/repository/price_repository"
	exceptions "oppaiman-backend-test/source/rest/exceptions"

	"gorm.io/gorm"
)

var ERR_FIND_PRICE = "Error while trying to find price with error: %s"
var ERR_DELETE_PRICE = "Error ocurred while trying to delete price %d with error: %s"
var ERR_UPDATE_PRICE = "Error ocurred while trying to update price %d with error: %s"
var ERR_CREATE_PRICE = "Error while trying to create new price with error: %s"
var ERR_PRICE_NOT_FOUND = "Price with id {%d} was not found"
var ERR_PRICE_DUPLICATE_REGION = "Price with region {%s} already exists"

func ListPrices() []*p.PriceResponse {
	price, err := rp.ListPrices()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_PRICE, err)

	}

	priceResponse := []*p.PriceResponse{}
	for _, price := range price {
		priceResponse = append(priceResponse, p.MapToPriceResponse(&price))
	}
	return priceResponse
}

func FindPriceById(id uint) *p.PriceResponse {
	price, err := rp.FindPriceById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_PRICE_NOT_FOUND, id)
	}

	return p.MapToPriceResponse(price)
}

func CreatePrice(req *p.PriceRequest) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		nameExists := rp.ExistsPriceByRegion(req.Region, tx)

		if nameExists {
			return exceptions.BadRequestException(ERR_PRICE_DUPLICATE_REGION, req.Region)
		}

		price := &p.Price{
			Region: req.Region,
			Value:  req.Value,
		}

		if err := rp.CreatePrice(price, tx); err != nil {
			return exceptions.InternalServerException(ERR_CREATE_PRICE, err)
		}

		return nil
	})
}

func UpdatePrice(req *p.PriceRequest, id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		price, err := rp.FindPriceById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_PRICE_NOT_FOUND, id)
		}

		price.Value = req.Value
		price.Region = req.Region

		if err := rp.UpdatePrice(price, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_PRICE, id, err)
		}

		return nil
	})
}

func DeletePrice(id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeletePrice(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_PRICE_NOT_FOUND, id)
			}

			return exceptions.InternalServerException(ERR_DELETE_PRICE, id, err)
		}

		return nil
	})
}
