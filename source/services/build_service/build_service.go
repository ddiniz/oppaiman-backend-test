package build_service

import (
	"errors"
	b "oppaiman-backend-test/source/domain/build"
	rp "oppaiman-backend-test/source/repository/build_repository"
	db "oppaiman-backend-test/source/repository/database"
	exceptions "oppaiman-backend-test/source/rest/exceptions"

	"gorm.io/gorm"
)

var ERR_FIND_BUILD = "Error while trying to find build with error: %s"
var ERR_DELETE_BUILD = "Error ocurred while trying to delete build %d with error: %s"
var ERR_UPDATE_BUILD = "Error ocurred while trying to update build %d with error: %s"
var ERR_CREATE_BUILD = "Error while trying to create new build with error: %s"
var ERR_BUILD_NOT_FOUND = "Build with id {%d} was not found"
var ERR_BUILD_DUPLICATE_NAME = "Build with name {%s} already exists"
var ERR_BUILD_DUPLICATE_VERSION = "Build with version {%s} already exists"

func ListBuilds() []*b.BuildResponse {
	build, err := rp.ListBuilds()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_BUILD, err)
	}

	buildResponse := []*b.BuildResponse{}
	for _, build := range build {
		buildResponse = append(buildResponse, b.MapToBuildResponse(&build))
	}
	return buildResponse
}

func FindBuildById(id uint) *b.BuildResponse {
	build, err := rp.FindBuildById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_BUILD_NOT_FOUND, id)
	}

	return b.MapToBuildResponse(build)
}

func CreateBuild(req *b.BuildRequest) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		nameExists := rp.ExistsBuildByName(req.Name, tx)

		if nameExists {
			return exceptions.BadRequestException(ERR_BUILD_DUPLICATE_NAME, req.Name)
		}

		versionExists := rp.ExistsBuildByVersion(req.Version, tx)

		if versionExists {
			return exceptions.BadRequestException(ERR_BUILD_DUPLICATE_VERSION, req.Version)
		}

		build := &b.Build{
			Name: req.Name,
			File: req.File,
		}

		if err := rp.CreateBuild(build, tx); err != nil {
			return exceptions.InternalServerException(ERR_CREATE_BUILD, err)
		}

		return nil
	})
}

func UpdateBuild(req *b.BuildRequest, id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		build, err := rp.FindBuildById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_BUILD_NOT_FOUND, id)
		}

		build.File = req.File
		build.Name = req.Name

		if err := rp.UpdateBuild(build, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_BUILD, id, err)
		}

		return nil
	})
}

func DeleteBuild(id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeleteBuild(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_BUILD_NOT_FOUND, id)
			}

			return exceptions.InternalServerException(ERR_DELETE_BUILD, id, err)
		}

		return nil
	})
}
