package os_service

import (
	"errors"
	"oppaiman-backend-test/source/domain/game_os"
	db "oppaiman-backend-test/source/repository/database"
	rp "oppaiman-backend-test/source/repository/os_repository"
	exceptions "oppaiman-backend-test/source/rest/exceptions"

	"gorm.io/gorm"
)

var ERR_FIND_OperatingSystem = "Error while trying to find OperatingSystem with error: %s"
var ERR_DELETE_OperatingSystem = "Error ocurred while trying to delete OperatingSystem %d with error: %s"
var ERR_UPDATE_OperatingSystem = "Error ocurred while trying to update OperatingSystem %d with error: %s"
var ERR_CREATE_OperatingSystem = "Error while trying to create new OperatingSystem with error: %s"
var ERR_OperatingSystem_NOT_FOUND = "OperatingSystem with id {%d} was not found"
var ERR_OperatingSystem_DUPLICATE_NAME = "OperatingSystem with name {%s} already exists"

func ListOperatingSystems() []*game_os.OperatingSystemResponse {
	os, err := rp.ListOperatingSystems()

	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FIND_OperatingSystem, err)

	}

	osResponse := []*game_os.OperatingSystemResponse{}
	for _, os := range os {
		osResponse = append(osResponse, game_os.MapToOperatingSystemResponse(&os))
	}
	return osResponse
}

func FindOperatingSystemById(id uint) *game_os.OperatingSystemResponse {
	os, err := rp.FindOperatingSystemById(id)
	if err != nil {
		exceptions.ThrowNotFoundException(ERR_OperatingSystem_NOT_FOUND, id)
	}

	return game_os.MapToOperatingSystemResponse(os)
}

func CreateOperatingSystem(req *game_os.OperatingSystemRequest) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		nameExists := rp.ExistsOperatingSystemByName(req.Name, tx)

		if nameExists {
			return exceptions.BadRequestException(ERR_OperatingSystem_DUPLICATE_NAME, req.Name)
		}

		os := &game_os.OperatingSystem{
			Description: req.Description,
			Name:        req.Name,
		}

		if err := rp.CreateOperatingSystem(os, tx); err != nil {
			return exceptions.InternalServerException(ERR_CREATE_OperatingSystem, err)
		}

		return nil
	})
}

func FindOrCreate(req *game_os.OperatingSystemRequest) *game_os.OperatingSystem {
	os, err := rp.FindOperatingSystemByName(req.Name)
	if err != nil {
		db.UsingTransactional(func(tx *db.TransactionalOperation) error {
			nameExists := rp.ExistsOperatingSystemByName(req.Name, tx)

			if nameExists {
				return nil
			}

			os := &game_os.OperatingSystem{
				Description: req.Description,
				Name:        req.Name,
			}

			if err := rp.CreateOperatingSystem(os, tx); err != nil {
				return exceptions.InternalServerException(ERR_CREATE_OperatingSystem, err)
			}

			return nil
		})
		os, err = rp.FindOperatingSystemByName(req.Name)
		if err != nil {
			exceptions.ThrowNotFoundException(ERR_OperatingSystem_NOT_FOUND, req.ID)
		}
	}
	return os
}

func UpdateOperatingSystem(req *game_os.OperatingSystemRequest, id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		os, err := rp.FindOperatingSystemById(id)
		if err != nil {
			return exceptions.NotFoundException(ERR_OperatingSystem_NOT_FOUND, id)
		}

		os.Description = req.Description
		os.Name = req.Name

		if err := rp.UpdateOperatingSystem(os, tx); err != nil {
			return exceptions.InternalServerException(ERR_UPDATE_OperatingSystem, id, err)
		}

		return nil
	})
}
func DeleteOperatingSystem(id uint) {
	db.UsingTransactional(func(tx *db.TransactionalOperation) error {
		if err := rp.DeleteOperatingSystem(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exceptions.NotFoundException(ERR_OperatingSystem_NOT_FOUND, id)
			}

			return exceptions.InternalServerException(ERR_DELETE_OperatingSystem, id, err)
		}

		return nil
	})
}
