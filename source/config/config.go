package config

import (
	"fmt"
	"time"

	"github.com/spf13/viper"
)

func ReadConfigFile() {
	viper.SetConfigType("json")
	viper.SetConfigName("config")
	viper.AddConfigPath("./")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
}

func GetDatabaseDriver() string {
	return viper.GetString("database.driver")
}
func GetDatabaseName() string {
	return viper.GetString("database.name")
}
func GetDatabasePassword() string {
	return viper.GetString("database.password")
}
func GetDatabaseSource() string {
	return viper.GetString("database.source")
}
func GetDatabaseUsername() string {
	return viper.GetString("database.username")
}

func GetGetJwtIdentityKey() string {
	return viper.GetString("jwt.identity_key")
}
func GetJwtRealm() string {
	return viper.GetString("jwt.realm")
}
func GetJwtSecretKey() []byte {
	return []byte(viper.GetString("jwt.secret_key"))
}
func GetJwtTimeout() time.Duration {
	return time.Duration(viper.GetInt64("jwt.timeout"))
}
func GetJwtMaxRefresh() time.Duration {
	return time.Duration(viper.GetInt64("jwt.max_refresh"))
}
