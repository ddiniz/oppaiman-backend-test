package rest

import (
	"net/http"
	"oppaiman-backend-test/source/domain/game_os"
	"oppaiman-backend-test/source/domain/user"
	msg "oppaiman-backend-test/source/language/messages"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/os_service"
	"strconv"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesOperatingSystem(router *gin.Engine) {
	oses := router.Group("/oses")
	oses.GET("", ListOperatingSystems)
	oses.GET("/:id", FindOperatingSystemById)
}

func InitializeRestRoutesOperatingSystemDev(router *gin.Engine) {
	var developerGuard = []user.Roles{user.DEVELOPER, user.ADMIN}
	oses := router.Group("/oses_dev")
	oses.Use(authentication.LoginGuard())
	oses.Use(authentication.RoleGuard(developerGuard))
	oses.POST("", CreateOperatingSystem)
	oses.PUT("/:id", UpdateOperatingSystem)
	oses.DELETE("/:id", DeleteOperatingSystem)
}

func ListOperatingSystems(c *gin.Context) {
	oses := sv.ListOperatingSystems()
	c.JSON(http.StatusOK, oses)
}

func FindOperatingSystemById(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}

	game := sv.FindOperatingSystemById(uint(id))
	c.JSON(http.StatusOK, game)
}

func CreateOperatingSystem(c *gin.Context) {
	newOperatingSystem := game_os.OperatingSystemRequest{}
	rest_utils.ReadBody(c, &newOperatingSystem)
	sv.CreateOperatingSystem(&newOperatingSystem)
	c.Status(http.StatusOK)
}

func UpdateOperatingSystem(c *gin.Context) {
	updateOperatingSystemRequest := game_os.OperatingSystemRequest{}
	rest_utils.ReadBody(c, &updateOperatingSystemRequest)
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.UpdateOperatingSystem(&updateOperatingSystemRequest, uint(id))
	c.Status(http.StatusOK)
}

func DeleteOperatingSystem(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.DeleteOperatingSystem(uint(id))
	c.Status(http.StatusOK)
}
