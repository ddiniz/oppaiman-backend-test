package rest

import (
	"net/http"
	g "oppaiman-backend-test/source/domain/price"
	"oppaiman-backend-test/source/domain/user"
	msg "oppaiman-backend-test/source/language/messages"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/price_service"
	"strconv"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesPrice(router *gin.Engine) {
	prices := router.Group("/prices")
	prices.GET("", ListPrices)
	prices.GET("/:id", FindPriceById)
}

func InitializeRestRoutesPriceDev(router *gin.Engine) {
	var developerGuard = []user.Roles{user.DEVELOPER, user.ADMIN}
	prices := router.Group("/prices_dev")
	prices.Use(authentication.LoginGuard())
	prices.Use(authentication.RoleGuard(developerGuard))
	prices.POST("", CreatePrice)
	prices.PUT("/:id", UpdatePrice)
	prices.DELETE("/:id", DeletePrice)
}

func ListPrices(c *gin.Context) {
	prices := sv.ListPrices()
	c.JSON(http.StatusOK, prices)
}

func FindPriceById(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}

	game := sv.FindPriceById(uint(id))
	c.JSON(http.StatusOK, game)
}

func CreatePrice(c *gin.Context) {
	newPrice := g.PriceRequest{}
	rest_utils.ReadBody(c, &newPrice)
	sv.CreatePrice(&newPrice)
	c.Status(http.StatusOK)
}

func UpdatePrice(c *gin.Context) {
	updatePriceRequest := g.PriceRequest{}
	rest_utils.ReadBody(c, &updatePriceRequest)
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.UpdatePrice(&updatePriceRequest, uint(id))
	c.Status(http.StatusOK)
}

func DeletePrice(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.DeletePrice(uint(id))
	c.Status(http.StatusOK)
}
