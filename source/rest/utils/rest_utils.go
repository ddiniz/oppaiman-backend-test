package rest_utils

import (
	rest_exceptions "oppaiman-backend-test/source/rest/exceptions"
	"strconv"

	"github.com/gin-gonic/gin"
)

func ReadBody(c *gin.Context, requestBody interface{}) {
	err := c.ShouldBindJSON(&requestBody)

	if err != nil {
		rest_exceptions.ThrowBadRequestException(err.Error())
	}
}

func ConvertToUInt(stringValue string) uint {
	return uint(ConvertToUInt64(stringValue))
}

func ConvertToUInt64(stringValue string) uint64 {
	valueConv, err := strconv.ParseUint(stringValue, 10, 64)

	if err != nil {
		rest_exceptions.ThrowBadRequestException("Error converting parameter to int with error: %s", err)
	}

	return valueConv
}
