package rest

import (
	"net/http"
	g "oppaiman-backend-test/source/domain/build"
	"oppaiman-backend-test/source/domain/user"
	msg "oppaiman-backend-test/source/language/messages"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/build_service"
	"strconv"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesBuild(router *gin.Engine) {
	builds := router.Group("/builds")
	builds.GET("", ListBuilds)
	builds.GET("/:id", FindBuildById)
}

func InitializeRestRoutesBuildDev(router *gin.Engine) {
	var developerGuard = []user.Roles{user.DEVELOPER, user.ADMIN}
	builds := router.Group("/builds_dev")
	builds.Use(authentication.LoginGuard())
	builds.Use(authentication.RoleGuard(developerGuard))
	builds.POST("", CreateBuild)
	builds.PUT("/:id", UpdateBuild)
	builds.DELETE("/:id", DeleteBuild)
}

func ListBuilds(c *gin.Context) {
	builds := sv.ListBuilds()
	c.JSON(http.StatusOK, builds)
}

func FindBuildById(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}

	game := sv.FindBuildById(uint(id))
	c.JSON(http.StatusOK, game)
}

func CreateBuild(c *gin.Context) {
	newBuild := g.BuildRequest{}
	rest_utils.ReadBody(c, &newBuild)
	sv.CreateBuild(&newBuild)
	c.Status(http.StatusOK)
}

func UpdateBuild(c *gin.Context) {
	updateBuildRequest := g.BuildRequest{}
	rest_utils.ReadBody(c, &updateBuildRequest)
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.UpdateBuild(&updateBuildRequest, uint(id))
	c.Status(http.StatusOK)
}

func DeleteBuild(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.DeleteBuild(uint(id))
	c.Status(http.StatusOK)
}
