package rest_exceptions

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type HttpException struct {
	StatusCode int    `example:"400"`
	Message    string `example:"Invalid path parameter"`
}

// TODO: find a better place to put this
func ExceptionMiddleware(c *gin.Context, recovered interface{}) {
	if exception, ok := recovered.(*HttpException); ok {
		c.String(exception.StatusCode, exception.Message)
	} else {
		log.Printf("Exception not mapped: %s", recovered)
		c.AbortWithStatus(http.StatusInternalServerError)
	}
}

func (h *HttpException) Error() string {
	return h.Message
}

func NotFoundException(format string, a ...any) *HttpException {
	return &HttpException{
		StatusCode: http.StatusNotFound,
		Message:    fmt.Sprintf(format, a...),
	}
}

func BadRequestException(format string, a ...any) *HttpException {
	return &HttpException{
		StatusCode: http.StatusBadRequest,
		Message:    fmt.Sprintf(format, a...),
	}
}

func InternalServerException(format string, a ...any) *HttpException {
	return &HttpException{
		StatusCode: http.StatusInternalServerError,
		Message:    fmt.Sprintf(format, a...),
	}
}
