package rest_exceptions

func ThrowNotFoundException(format string, a ...any) {
	panic(NotFoundException(format, a...))
}

func ThrowBadRequestException(format string, a ...any) {
	panic(BadRequestException(format, a...))
}

func ThrowInternalServerException(format string, a ...any) {
	panic(InternalServerException(format, a...))
}
