package rest

import (
	"net/http"
	g "oppaiman-backend-test/source/domain/image"
	"oppaiman-backend-test/source/domain/user"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/image_service"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesImage(router *gin.Engine) {
	images := router.Group("/images")
	images.GET("", ListImages)
	images.GET("/:id", FindImageById)
}

func InitializeRestRoutesImageDev(router *gin.Engine) {
	var developerGuard = []user.Roles{user.DEVELOPER, user.ADMIN}
	images := router.Group("/images_dev")
	images.Use(authentication.LoginGuard())
	images.Use(authentication.RoleGuard(developerGuard))
	images.POST("", CreateImage)
	images.PUT("/:id", UpdateImage)
	images.DELETE("/:id", DeleteImage)
}

func ListImages(c *gin.Context) {
	images := sv.ListImages()
	c.JSON(http.StatusOK, images)
}

func FindImageById(c *gin.Context) {
	id := c.Params.ByName("id")

	game := sv.FindImageById(id)
	c.JSON(http.StatusOK, game)
}

func CreateImage(c *gin.Context) {
	newImage := g.ImageRequest{}
	rest_utils.ReadBody(c, &newImage)
	sv.CreateImage(&newImage)
	c.Status(http.StatusOK)
}

func UpdateImage(c *gin.Context) {
	updateImageRequest := g.ImageRequest{}
	rest_utils.ReadBody(c, &updateImageRequest)
	id := c.Params.ByName("id")
	sv.UpdateImage(&updateImageRequest, id)
	c.Status(http.StatusOK)
}

func DeleteImage(c *gin.Context) {
	id := c.Params.ByName("id")
	sv.DeleteImage(id)
	c.Status(http.StatusOK)
}
