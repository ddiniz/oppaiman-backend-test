package health

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesHealth(router *gin.Engine) {
	games := router.Group("/health")
	games.GET("", Health)
}

// Health godoc
// @Summary     Health endpoint
// @Description Discover application health
// @Tags        Actuator
// @Accept      json
// @Produce     json
// @Success     200 {object} string "{"status": "OK"}"
// @Router      /health [get]
func Health(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
	})
}
