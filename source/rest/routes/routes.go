package routes

import (
	"log"
	"oppaiman-backend-test/source/repository/file_repository"
	login_rest "oppaiman-backend-test/source/rest/authentication_rest"
	build_rest "oppaiman-backend-test/source/rest/build_rest"
	rest_exceptions "oppaiman-backend-test/source/rest/exceptions"
	game_rest "oppaiman-backend-test/source/rest/game_rest"
	genre_rest "oppaiman-backend-test/source/rest/genre_rest"
	health_rest "oppaiman-backend-test/source/rest/health_rest"
	image_rest "oppaiman-backend-test/source/rest/image_rest"
	os_rest "oppaiman-backend-test/source/rest/os_rest"
	price_rest "oppaiman-backend-test/source/rest/price_rest"
	user_rest "oppaiman-backend-test/source/rest/user_rest"

	"github.com/gin-gonic/gin"
)

func SetupRouterRoutes(router *gin.Engine) {
	router.Use(gin.CustomRecovery(rest_exceptions.ExceptionMiddleware))

	appPath, err := file_repository.GetApplicationPath()
	if err != nil {
		log.Fatalln(err)
	}
	media := appPath + "/" + file_repository.PUBLIC_FOLDER
	router.Static("/Content", media)

	login_rest.InitializeRestRoutesAuthentication(router)

	health_rest.InitializeRestRoutesHealth(router)

	user_rest.InitializeRestRoutesNewUser(router)
	user_rest.InitializeRestRoutesUserSelf(router)
	user_rest.InitializeRestRoutesUserAdmin(router)

	game_rest.InitializeRestRoutesGame(router)
	game_rest.InitializeRestRoutesGameUser(router)
	game_rest.InitializeRestRoutesGameDev(router)

	image_rest.InitializeRestRoutesImage(router)
	image_rest.InitializeRestRoutesImageDev(router)

	genre_rest.InitializeRestRoutesGenre(router)
	genre_rest.InitializeRestRoutesGenreAdmin(router)

	os_rest.InitializeRestRoutesOperatingSystem(router)
	os_rest.InitializeRestRoutesOperatingSystemDev(router)

	build_rest.InitializeRestRoutesBuild(router)
	build_rest.InitializeRestRoutesBuildDev(router)

	price_rest.InitializeRestRoutesPrice(router)
	price_rest.InitializeRestRoutesPriceDev(router)
}
