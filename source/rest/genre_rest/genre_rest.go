package rest

import (
	"net/http"
	g "oppaiman-backend-test/source/domain/genre"
	"oppaiman-backend-test/source/domain/user"
	msg "oppaiman-backend-test/source/language/messages"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/genre_service"
	"strconv"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesGenre(router *gin.Engine) {
	games := router.Group("/genres")
	games.GET("", ListGenres)
	games.GET("/:id", FindGenreById)
}

func InitializeRestRoutesGenreAdmin(router *gin.Engine) {
	var adminGuard = []user.Roles{user.ADMIN}
	games := router.Group("/genres_admin")
	games.Use(authentication.LoginGuard())
	games.Use(authentication.RoleGuard(adminGuard))
	games.POST("", CreateGenre)
	games.PUT("/:id", UpdateGenre)
	games.DELETE("/:id", DeleteGenre)
}

func ListGenres(c *gin.Context) {
	games := sv.ListGenres()
	c.JSON(http.StatusOK, games)
}

func FindGenreById(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}

	game := sv.FindGenreById(uint(id))
	c.JSON(http.StatusOK, game)
}

func CreateGenre(c *gin.Context) {
	newGenre := g.GenreRequest{}
	rest_utils.ReadBody(c, &newGenre)
	sv.CreateGenre(&newGenre)
	c.Status(http.StatusOK)
}

func UpdateGenre(c *gin.Context) {
	GenreRequest := g.GenreRequest{}
	rest_utils.ReadBody(c, &GenreRequest)
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.UpdateGenre(&GenreRequest, uint(id))
	c.Status(http.StatusOK)
}

func DeleteGenre(c *gin.Context) {
	id, err := strconv.ParseInt(c.Params.ByName("id"), 10, 64)
	if err != nil {
		exceptions.ThrowInternalServerException(msg.ERR_MSG_INVALID_PARAMETER, err)
	}
	sv.DeleteGenre(uint(id))
	c.Status(http.StatusOK)
}
