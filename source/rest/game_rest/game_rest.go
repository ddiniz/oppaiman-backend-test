package rest

import (
	"encoding/json"
	"mime/multipart"
	"net/http"
	g "oppaiman-backend-test/source/domain/game"
	"oppaiman-backend-test/source/domain/user"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/game_service"
	"oppaiman-backend-test/source/services/user_service"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesGame(router *gin.Engine) {
	games := router.Group("/games")
	games.GET("", ListGames)
	games.GET("/paginated", FindGamesPaginated)
	games.GET("/:id", FindGameById)
	games.GET("/page/:pageUrl", FindGameByPageUrl)
}

func InitializeRestRoutesGameUser(router *gin.Engine) {
	var userGuard = []user.Roles{user.NORMAL, user.DEVELOPER, user.ADMIN}
	games := router.Group("/games")
	games.Use(authentication.LoginGuard())
	games.Use(authentication.RoleGuard(userGuard))
	games.GET("/user", ListUserGames)
}

func InitializeRestRoutesGameDev(router *gin.Engine) {
	var developerGuard = []user.Roles{user.DEVELOPER, user.ADMIN}
	games := router.Group("/games_dev")
	games.Use(authentication.LoginGuard())
	games.Use(authentication.RoleGuard(developerGuard))
	games.POST("", CreateGame)
	games.PUT("/:id", UpdateGame)
	games.DELETE("/:id", DeleteGame)
}

func ListGames(c *gin.Context) {
	games := sv.ListGames()
	c.JSON(http.StatusOK, games)
}
func ListUserGames(c *gin.Context) {
	user := user_service.FindSelf(c)
	games := sv.ListUserGames(user.ID)
	c.JSON(http.StatusOK, games)
}

var ERR_MALFORMED_PARAMETER = "Bad request."

func FindGamesPaginated(c *gin.Context) {
	var request g.FilterGameRequest
	if c.BindQuery(&request) == nil {
		c.Status(http.StatusBadRequest)
	}

	filter := g.MapToFilterGame(&request)
	games := sv.FindGamesPaginated(*filter)
	c.JSON(http.StatusOK, games)
}
func FindGameById(c *gin.Context) {
	id := c.Params.ByName("id")
	game := sv.FindGameById(id)
	c.JSON(http.StatusOK, game)
}
func FindGameByPageUrl(c *gin.Context) {
	pageUrl := c.Params.ByName("pageUrl")
	game := sv.FindGameByPageUrl(pageUrl)
	c.JSON(http.StatusOK, game)
}
func CreateGame(c *gin.Context) {
	form, _ := c.MultipartForm()
	game := &g.GameRequest{}
	err := json.Unmarshal([]byte(form.Value["gamedata"][0]), &game)
	if err != nil {
		c.Status(http.StatusBadRequest)
	}
	var catalogImage *multipart.FileHeader
	var headerImage *multipart.FileHeader
	var gameFile *multipart.FileHeader
	screenshot := make([]*multipart.FileHeader, 0)
	for filename, file := range form.File {
		if len(file) > 0 {
			switch filename {
			case "headerImage":
				headerImage = file[0]
			case "catalogImage":
				catalogImage = file[0]
			case "screenshot0", "screenshot1", "screenshot2", "screenshot3", "screenshot4":
				screenshot = append(screenshot, file[0])
			case "game":
				gameFile = file[0]
			}
		}
	}
	sv.CreateGame(c, game, gameFile, catalogImage, headerImage, screenshot)
	c.Status(http.StatusOK)
}

func UpdateGame(c *gin.Context) {
	updateGameRequest := g.GameRequest{}
	rest_utils.ReadBody(c, &updateGameRequest)
	id := c.Params.ByName("id")
	sv.UpdateGame(&updateGameRequest, id)
	c.Status(http.StatusOK)
}
func DeleteGame(c *gin.Context) {
	id := c.Params.ByName("id")
	sv.DeleteGame(id)
	c.Status(http.StatusOK)
}
