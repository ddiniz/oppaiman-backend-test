package rest

import (
	"oppaiman-backend-test/source/services/authentication"

	"github.com/gin-gonic/gin"
)

func InitializeRestRoutesAuthentication(router *gin.Engine) {
	router.POST("/login", authentication.Jwt.LoginHandler)
}

// TODO: solve the import loop that would happen if the login endpoint (in authentication.go) was declared in this file
