package rest

import (
	"fmt"
	"net/http"
	u "oppaiman-backend-test/source/domain/user"
	exceptions "oppaiman-backend-test/source/rest/exceptions"
	rest_utils "oppaiman-backend-test/source/rest/utils"
	"oppaiman-backend-test/source/services/authentication"
	sv "oppaiman-backend-test/source/services/user_service"

	"github.com/gin-gonic/gin"
)

var ERR_FAILED_FILE_READ = "Failed to read file"
var ERR_FAILED_FILE_INFO = "Failed to get file info."

func InitializeRestRoutesNewUser(router *gin.Engine) {
	users := router.Group("/users")
	users.POST("", CreateUser)
}

func InitializeRestRoutesUserSelf(router *gin.Engine) {
	users := router.Group("/users_self")
	users.Use(authentication.LoginGuard())
	users.PUT("/buy_game/:id", BuyGame)
	users.GET("", FindSelf)
	users.GET("/game/:pageUrl", HasGame)
	users.GET("/getgame/:gameId", GetGame)
	users.PUT("", UpdateSelf)
	users.DELETE("", DeleteSelf)
}

func InitializeRestRoutesUserAdmin(router *gin.Engine) {
	users := router.Group("/users_admin")
	var adminGuard = []u.Roles{u.ADMIN}
	users.Use(authentication.LoginGuard())
	users.Use(authentication.RoleGuard(adminGuard))
	users.GET("", ListUsers)
	users.GET("/paginated", FindUsersPaginated)
	users.GET("/:id", FindUserById)
	users.GET("/username/:username", FindUserByUsername)
	users.GET("/email/:email", FindUserByEmail)
	users.PUT("/:id", UpdateUser)
	users.DELETE("/:id", DeleteUser)
}

func ListUsers(c *gin.Context) {
	users := sv.ListUsers()
	c.JSON(http.StatusOK, users)
}

func FindUsersPaginated(c *gin.Context) {
	limit := rest_utils.ConvertToUInt(c.DefaultQuery("limit", "10"))
	page := rest_utils.ConvertToUInt(c.DefaultQuery("page", "1"))
	users := sv.FindUsersPaginated(limit, page)
	c.JSON(http.StatusOK, users)
}

func FindUserById(c *gin.Context) {
	id := c.Params.ByName("id")
	user := sv.FindUserById(id)
	c.JSON(http.StatusOK, user)
}

func FindUserByUsername(c *gin.Context) {
	id := c.Params.ByName("username")
	user := sv.FindUserByUsername(id)
	c.JSON(http.StatusOK, user)
}
func FindUserByEmail(c *gin.Context) {
	email := c.Params.ByName("email")
	user := sv.FindUserByEmail(email)
	c.JSON(http.StatusOK, user)
}

func CreateUser(c *gin.Context) {
	newUser := u.UserRequest{}
	rest_utils.ReadBody(c, &newUser)
	sv.CreateUser(&newUser)
	c.Status(http.StatusOK)
}

func UpdateUser(c *gin.Context) {
	updateUserRequest := u.UserRequest{}
	rest_utils.ReadBody(c, &updateUserRequest)
	id := c.Params.ByName("id")
	sv.UpdateUser(&updateUserRequest, id)
	c.Status(http.StatusOK)
}

func DeleteUser(c *gin.Context) {
	id := c.Params.ByName("id")
	sv.DeleteUser(id)
	c.Status(http.StatusOK)
}

func DeleteSelf(c *gin.Context) {
	sv.DeleteSelf(c)
	c.Status(http.StatusOK)
}

func UpdateSelf(c *gin.Context) {
	updateUserRequest := u.UserRequest{}
	rest_utils.ReadBody(c, &updateUserRequest)
	sv.UpdateSelf(&updateUserRequest, c)
	c.Status(http.StatusOK)
}

func FindSelf(c *gin.Context) {
	user := sv.FindSelf(c)
	c.JSON(http.StatusOK, user)
}

func BuyGame(c *gin.Context) {
	gameId := c.Params.ByName("id")
	sv.BuyGame(gameId, c)
	c.Status(http.StatusOK)
}

func HasGame(c *gin.Context) {
	pageUrl := c.Params.ByName("pageUrl")
	hasGame := sv.HasGame(pageUrl, c)
	c.JSON(http.StatusOK, hasGame)
}

func GetGame(c *gin.Context) {
	gameId := c.Params.ByName("gameId")
	buildId := c.Params.ByName("buildId")
	gameFile, fileName, filePath := sv.GetGame(gameId, buildId, c)

	defer gameFile.Close()

	fileHeader := make([]byte, 512)
	_, err := gameFile.Read(fileHeader)
	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FAILED_FILE_READ)
	}
	fileContentType := http.DetectContentType(fileHeader)
	fileInfo, err := gameFile.Stat()
	if err != nil {
		exceptions.ThrowInternalServerException(ERR_FAILED_FILE_INFO)
	}
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fileName))
	c.Header("Content-Type", fileContentType)
	c.Header("Content-Length", fmt.Sprintf("%d", fileInfo.Size()))
	c.File(filePath)
}
