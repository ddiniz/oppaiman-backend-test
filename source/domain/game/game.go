package game

import (
	"oppaiman-backend-test/source/domain/build"
	"oppaiman-backend-test/source/domain/game_os"
	"oppaiman-backend-test/source/domain/genre"
	"oppaiman-backend-test/source/domain/image"
	"oppaiman-backend-test/source/domain/price"
	"oppaiman-backend-test/source/domain/usergame"
	"strconv"
	"time"

	"gorm.io/gorm"
)

type Game struct {
	ID        string `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`

	Description      string `gorm:"size:65535"`
	ShortDescription string `gorm:"size:1000"`
	Title            string `gorm:"unique"`
	PageUrl          string `gorm:"unique"`
	Status           string

	Prices           []price.Price
	Builds           []build.Build
	Images           []image.Image
	Genres           []genre.Genre             `gorm:"many2many:game_genre;"`
	OperatingSystems []game_os.OperatingSystem `gorm:"many2many:game_operating_system;"`

	CatalogImage image.Image
	HeaderImage  image.Image

	CreatorID uint
	Owners    []usergame.UserGame
}

type GameResponse struct {
	ID               string                             `json:"id"`
	CreatedAt        time.Time                          `json:"createdAt"`
	UpdatedAt        time.Time                          `json:"updatedAt"`
	DeletedAt        gorm.DeletedAt                     `json:"deletedAt"`
	Description      string                             `json:"description"`
	ShortDescription string                             `json:"shortDescription"`
	Title            string                             `json:"title"`
	PageUrl          string                             `json:"pageUrl"`
	Status           string                             `json:"status"`
	Prices           []*price.PriceResponse             `json:"prices"`
	Builds           []*build.BuildResponse             `json:"builds"`
	Images           []*image.ImageResponse             `json:"images"`
	Genres           []*genre.GenreResponse             `json:"genres"`
	OperatingSystems []*game_os.OperatingSystemResponse `json:"oses"`
	CatalogImage     *image.ImageResponse               `json:"catalogImage"`
	HeaderImage      *image.ImageResponse               `json:"headerImage"`
}

type GameRequest struct {
	Description      string                           `json:"description" binding:"required"`
	ShortDescription string                           `json:"shortDescription" binding:"required"`
	Title            string                           `json:"title" binding:"required"`
	PageUrl          string                           `json:"pageUrl" binding:"required"`
	Status           string                           `json:"status" binding:"required"`
	Prices           []price.PriceRequest             `json:"prices"`
	Builds           []build.BuildRequest             `json:"builds"`
	Genres           []genre.GenreRequest             `json:"genres"`
	OperatingSystems []game_os.OperatingSystemRequest `json:"oses"`
}

func MapToGameResponse(obj *Game) *GameResponse {
	prices := make([]*price.PriceResponse, len(obj.Prices))
	for i, pri := range obj.Prices {
		prices[i] = price.MapToPriceResponse(&pri)
	}
	builds := make([]*build.BuildResponse, len(obj.Builds))
	for i, pri := range obj.Builds {
		builds[i] = build.MapToBuildResponse(&pri)
	}
	images := make([]*image.ImageResponse, len(obj.Images))
	for i, pri := range obj.Images {
		images[i] = image.MapToImageResponse(&pri)
	}
	genres := make([]*genre.GenreResponse, len(obj.Genres))
	for i, pri := range obj.Genres {
		genres[i] = genre.MapToGenreResponse(&pri)
	}
	oses := make([]*game_os.OperatingSystemResponse, len(obj.OperatingSystems))
	for i, pri := range obj.OperatingSystems {
		oses[i] = game_os.MapToOperatingSystemResponse(&pri)
	}
	catalogImage := image.MapToImageResponse(&obj.CatalogImage)
	headerImage := image.MapToImageResponse(&obj.HeaderImage)
	return &GameResponse{
		ID:               obj.ID,
		CreatedAt:        obj.CreatedAt,
		UpdatedAt:        obj.UpdatedAt,
		DeletedAt:        obj.DeletedAt,
		Description:      obj.Description,
		ShortDescription: obj.ShortDescription,
		Title:            obj.Title,
		PageUrl:          obj.PageUrl,
		Status:           obj.Status,
		Prices:           prices,
		Builds:           builds,
		Images:           images,
		Genres:           genres,
		OperatingSystems: oses,
		CatalogImage:     catalogImage,
		HeaderImage:      headerImage,
	}
}

type FilterGameRequest struct {
	Name        string `form:"name" json:"name"`
	Price       string `form:"price" json:"price"`
	OnSale      string `form:"onSale" json:"onSale"`
	ReleaseDate string `form:"releaseDate" json:"releaseDate"`
	Genre       string `form:"genre" json:"genre"`
	Limit       string `form:"limit" json:"limit"`
	Page        string `form:"page" json:"page"`
}

type FilterGame struct {
	Name        string
	Price       float64
	OnSale      bool
	ReleaseDate time.Time
	Genre       string
	Limit       uint
	Page        uint
}

var ERR_MALFORMED_PARAMETER = "Parameter {%s} invalid, sent value {%s}"

func MapToFilterGame(obj *FilterGameRequest) *FilterGame {
	price, err := strconv.ParseFloat(obj.Price, 64)
	if err != nil {
		price = -1
	}
	onSale, err := strconv.ParseBool(obj.OnSale)
	if err != nil {
		onSale = false
	}
	releaseDate, err := strconv.ParseInt(obj.ReleaseDate, 10, 64)
	if err != nil {
		releaseDate = 0
	}
	limit, err := strconv.ParseInt(obj.Limit, 10, 32)
	if err != nil {
		limit = 10
	}
	page, err := strconv.ParseInt(obj.Page, 10, 32)
	if err != nil {
		page = 1
	}

	return &FilterGame{
		Name:        obj.Name,
		Price:       price,
		OnSale:      onSale,
		ReleaseDate: time.UnixMilli(releaseDate),
		Genre:       obj.Genre,
		Limit:       uint(limit),
		Page:        uint(page),
	}
}
