package image

import (
	"time"

	"gorm.io/gorm"
)

type Image struct {
	ID        string `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`

	Name string
	Src  string `gorm:"type:blob"`

	GameID string
}
type ImageRequest struct {
	Name string `json:"name"`
	Src  string `json:"src"`
}
type ImageResponse struct {
	ID        string         `json:"id"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `json:"deletedAt"`
	Name      string         `json:"name"`
	Src       string         `json:"src"`
}

func MapToImageResponse(obj *Image) *ImageResponse {
	return &ImageResponse{
		ID:   obj.ID,
		Name: obj.Name,
		Src:  obj.Src,
	}
}
