package game_os

type OperatingSystem struct {
	ID          uint   `gorm:"primaryKey"`
	Name        string `gorm:"unique"`
	Description string
}
type OperatingSystemRequest struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
type OperatingSystemResponse struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func MapToOperatingSystemResponse(obj *OperatingSystem) *OperatingSystemResponse {
	return &OperatingSystemResponse{
		ID:          obj.ID,
		Description: obj.Description,
		Name:        obj.Name,
	}
}
