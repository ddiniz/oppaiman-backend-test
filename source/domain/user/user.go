package user

import (
	"oppaiman-backend-test/source/domain/game"
	"oppaiman-backend-test/source/domain/usergame"
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID        string `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`

	Email         string `gorm:"unique"`
	Username      string `gorm:"unique"`
	Password      string
	Role          Roles
	AuthoredGames []game.Game

	BoughtGames []usergame.UserGame
}

type LoginRequest struct {
	UsernameOrEmail string `json:"usernameOrEmail" binding:"required" example:"admin"`
	Password        string `json:"password" binding:"required" example:"admin"`
}

type UserRequest struct {
	Username string `json:"username" binding:"required"`
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
	Role     Roles  `json:"role"`
}

type UserResponse struct {
	ID        string         `json:"id"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `json:"deletedAt"`
	Email     string         `json:"email"`
	Username  string         `json:"username"`
	Role      Roles          `json:"role"`
}

func MapToUserResponse(obj *User) *UserResponse {
	return &UserResponse{
		ID:        obj.ID,
		CreatedAt: obj.CreatedAt,
		UpdatedAt: obj.UpdatedAt,
		DeletedAt: obj.DeletedAt,
		Email:     obj.Email,
		Username:  obj.Username,
		Role:      obj.Role,
	}
}
