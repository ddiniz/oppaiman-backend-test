package user

type Roles uint

const (
	NORMAL Roles = iota
	DEVELOPER
	ADMIN
)
