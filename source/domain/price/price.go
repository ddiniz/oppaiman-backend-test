package price

import (
	"time"

	"gorm.io/gorm"
)

type Price struct {
	ID        uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`

	Region string
	Value  float64

	GameID string
}
type PriceRequest struct {
	Region string  `json:"region"`
	Value  float64 `json:"value"`
}

type PriceResponse struct {
	ID        uint           `json:"id"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `json:"deletedAt"`
	Region    string         `json:"region"`
	Value     float64        `json:"value"`
}

func MapToPriceResponse(obj *Price) *PriceResponse {
	return &PriceResponse{
		ID:        obj.ID,
		Region:    obj.Region,
		Value:     obj.Value,
		CreatedAt: obj.CreatedAt,
		UpdatedAt: obj.UpdatedAt,
		DeletedAt: obj.DeletedAt,
	}
}
