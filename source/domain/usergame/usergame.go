package usergame

import (
	"time"

	"gorm.io/gorm"
)

type UserGame struct {
	UserID    string `gorm:"primaryKey"`
	GameID    string `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
