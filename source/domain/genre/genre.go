package genre

type Genre struct {
	ID          uint   `gorm:"primaryKey"`
	Name        string `gorm:"unique"`
	Description string
}
type GenreRequest struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
type GenreResponse struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func MapToGenreResponse(obj *Genre) *GenreResponse {
	return &GenreResponse{
		ID:          obj.ID,
		Description: obj.Description,
		Name:        obj.Name,
	}
}
