package pagination

type Pagination struct {
	Limit      uint   `json:"limit"`
	Page       uint   `json:"page"`
	Sort       string `json:"sort"`
	TotalRows  uint64 `json:"total_rows"`
	TotalPages uint   `json:"total_pages"`
	Rows       any    `json:"rows"`
}

func (p *Pagination) GetOffset() uint {
	return (p.GetPage() - 1) * p.GetLimit()
}

func (p *Pagination) GetLimit() uint {
	if p.Limit <= 0 {
		p.Limit = 10
	}

	return p.Limit
}

func (p *Pagination) GetPage() uint {
	if p.Page <= 0 {
		p.Page = 1
	}
	return p.Page
}

func (p *Pagination) GetSort() string {
	if p.Sort == "" {
		p.Sort = "Id asc"
	}
	return p.Sort
}
