package build

import (
	"time"

	"gorm.io/gorm"
)

type Build struct {
	ID        uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`

	Name        string
	Description string
	Version     string
	File        string `gorm:"type:blob"`

	GameID string
}
type BuildRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Version     string `json:"version"`
	File        string `json:"file"`
}
type BuildResponse struct {
	ID          uint           `json:"id"`
	CreatedAt   time.Time      `json:"createdAt"`
	UpdatedAt   time.Time      `json:"updatedAt"`
	DeletedAt   gorm.DeletedAt `json:"deletedAt"`
	Name        string         `json:"name"`
	Description string         `json:"description"`
	Version     string         `json:"version"`
	File        string         `json:"file"`
}

func MapToBuildResponse(obj *Build) *BuildResponse {
	return &BuildResponse{
		ID:        obj.ID,
		Name:      obj.Name,
		File:      obj.File,
		CreatedAt: obj.CreatedAt,
		UpdatedAt: obj.UpdatedAt,
		DeletedAt: obj.DeletedAt,
	}
}
