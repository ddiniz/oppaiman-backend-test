package genre_repository

import (
	g "oppaiman-backend-test/source/domain/genre"
	db "oppaiman-backend-test/source/repository/database"

	"gorm.io/gorm"
)

func ListGenres(tx ...*db.TransactionalOperation) ([]g.Genre, error) {
	genres := []g.Genre{}
	return genres, db.WithTransaction(tx).Order("id asc").Find(&genres).Error
}

func FindGenreById(id uint, tx ...*db.TransactionalOperation) (*g.Genre, error) {
	genre := &g.Genre{}
	return genre, db.WithTransaction(tx).Where("id = ?", id).First(genre).Error
}

func FindGenreByName(name string, tx ...*db.TransactionalOperation) (*g.Genre, error) {
	genre := &g.Genre{}
	return genre, db.WithTransaction(tx).Where("name = ?", name).First(genre).Error
}

func ExistsGenreByName(name string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("name = ?", name).Find(&g.Genre{})
	return dbResult.RowsAffected > 0
}

func CreateGenre(genre *g.Genre, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Create(genre).Error
}

func UpdateGenre(genre *g.Genre, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Model(&g.Genre{}).Updates(genre).Error
}

func DeleteGenre(id uint, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(g.Genre{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
