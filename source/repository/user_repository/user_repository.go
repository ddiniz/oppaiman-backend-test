package user_repository

import (
	p "oppaiman-backend-test/source/domain/pagination"
	u "oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/domain/usergame"
	db "oppaiman-backend-test/source/repository/database"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

func FindUsersPaginated(limit uint, page uint, tx ...*db.TransactionalOperation) (*p.Pagination, error) {
	pagination := &p.Pagination{Limit: limit, Page: page}
	users := []u.User{}

	transaction := db.WithTransaction(tx)
	err := transaction.Scopes(db.PaginateScope(users, pagination, transaction)).Order("username asc").Find(&users).Error

	pagination.Rows = users

	return pagination, err
}

func ListUsers(tx ...*db.TransactionalOperation) ([]u.User, error) {
	users := []u.User{}
	return users, db.WithTransaction(tx).Order("username asc").Find(&users).Error
}

func FindUserById(id string, tx ...*db.TransactionalOperation) (*u.User, error) {
	user := &u.User{}
	return user, db.WithTransaction(tx).Where("id = ?", id).First(user).Error
}
func FindUserByUsername(username string, tx ...*db.TransactionalOperation) (*u.User, error) {
	user := &u.User{}
	return user, db.WithTransaction(tx).Where("username = ?", username).First(user).Error
}
func FindUserByEmail(email string, tx ...*db.TransactionalOperation) (*u.User, error) {
	user := &u.User{}
	return user, db.WithTransaction(tx).Where("email = ?", email).First(user).Error
}

func ExistsUserByUsername(username string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("username = ?", username).Find(&u.User{})
	return dbResult.RowsAffected > 0
}

func ExistsUserByEmail(email string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("email = ?", email).Find(&u.User{})
	return dbResult.RowsAffected > 0
}

func CreateUser(user *u.User, tx ...*db.TransactionalOperation) error {
	user.ID = uuid.NewString()
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	user.Password = string(hash)
	if err != nil {
		return err
	}
	return db.WithTransaction(tx).Create(user).Error
}

func UpdateUser(user *u.User, tx ...*db.TransactionalOperation) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	return db.WithTransaction(tx).Model(&u.User{}).Updates(user).Error
}

func DeleteUser(id string, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(u.User{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}

func UserHasGame(userId, gameId string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("user_id = ?", userId).Where("game_id = ?", gameId).Find(&usergame.UserGame{})
	return dbResult.RowsAffected > 0
}
