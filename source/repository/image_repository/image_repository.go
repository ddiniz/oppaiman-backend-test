package image_repository

import (
	s "oppaiman-backend-test/source/domain/image"
	db "oppaiman-backend-test/source/repository/database"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func ListImages(tx ...*db.TransactionalOperation) ([]s.Image, error) {
	images := []s.Image{}
	return images, db.WithTransaction(tx).Order("id asc").Find(&images).Error
}

func FindImageById(id string, tx ...*db.TransactionalOperation) (*s.Image, error) {
	image := &s.Image{}
	return image, db.WithTransaction(tx).Where("id = ?", id).First(image).Error
}
func ExistsImageByName(name string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("name = ?", name).Find(&s.Image{})
	return dbResult.RowsAffected > 0
}

func CreateImage(image *s.Image, tx ...*db.TransactionalOperation) error {
	image.ID = uuid.NewString()
	return db.WithTransaction(tx).Create(image).Error
}

func UpdateImage(image *s.Image, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Model(&s.Image{}).Updates(image).Error
}

func DeleteImage(id string, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(s.Image{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
