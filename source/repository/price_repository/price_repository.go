package price_repository

import (
	p "oppaiman-backend-test/source/domain/price"
	db "oppaiman-backend-test/source/repository/database"

	"gorm.io/gorm"
)

func ListPrices(tx ...*db.TransactionalOperation) ([]p.Price, error) {
	prices := []p.Price{}
	return prices, db.WithTransaction(tx).Order("id asc").Find(&prices).Error
}

func FindPriceById(id uint, tx ...*db.TransactionalOperation) (*p.Price, error) {
	price := &p.Price{}
	return price, db.WithTransaction(tx).Where("id = ?", id).First(price).Error
}

func ExistsPriceByRegion(region string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("region = ?", region).Find(&p.Price{})
	return dbResult.RowsAffected > 0
}

func CreatePrice(price *p.Price, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Create(price).Error
}

func UpdatePrice(price *p.Price, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Model(&p.Price{}).Updates(price).Error
}

func DeletePrice(id uint, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(p.Price{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
