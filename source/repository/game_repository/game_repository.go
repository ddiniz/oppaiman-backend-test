package game_repository

import (
	g "oppaiman-backend-test/source/domain/game"
	p "oppaiman-backend-test/source/domain/pagination"
	db "oppaiman-backend-test/source/repository/database"

	"gorm.io/gorm"
)

func FindGamesPaginated(filter g.FilterGame, tx ...*db.TransactionalOperation) (*p.Pagination, error) {
	pagination := &p.Pagination{Limit: filter.Limit, Page: filter.Page}
	games := []g.Game{}

	transaction := db.WithTransaction(tx).
		Joins("inner join prices p on games.id = p.game_id").
		Joins("inner join game_genre gg on gg.game_id = games.id").
		Joins("inner join genres g on g.id = gg.genre_id")

	if filter.Price != -1 {
		transaction = transaction.Where("p.value <= ?", filter.Price)
	}

	if filter.ReleaseDate.UnixMilli() != 0 {
		transaction = transaction.Where("games.created_at >= ?", filter.ReleaseDate)
	}
	if len(filter.Genre) > 0 {
		transaction = transaction.Where("g.name IN (?)", filter.Genre)
	}

	if len(filter.Name) > 0 {
		transaction = transaction.Where("title = ?", filter.Name)
	}

	err := transaction.
		Find(&games).Error
	if err != nil {
		return pagination, err
	}

	gameIdsMap := make(map[string]string)
	for _, game := range games {
		gameIdsMap[game.ID] = game.ID
	}
	gameIds := make([]string, 0)
	for _, id := range gameIdsMap {
		gameIds = append(gameIds, id)
	}

	transaction = db.WithTransaction(tx)
	err = transaction.Scopes(db.PaginateScope(games, pagination, transaction)).
		Preload("Prices").
		Preload("Builds").
		Preload("Genres").
		Preload("OperatingSystems").
		Preload("Images").
		Preload("HeaderImage").
		Preload("CatalogImage").
		Where("id IN(?)", gameIds).
		Order("title asc").
		Find(&games).Error

	pagination.Rows = games

	return pagination, err
}

func ListGames(tx ...*db.TransactionalOperation) ([]g.Game, error) {
	games := []g.Game{}
	return games, db.WithTransaction(tx).
		Preload("Prices").
		Preload("Builds").
		Preload("Genres").
		Preload("OperatingSystems").
		Order("title asc").
		Find(&games).
		Error
}

func ListUserGames(userID string, tx ...*db.TransactionalOperation) ([]g.Game, error) {
	games := []g.Game{}
	return games, db.WithTransaction(tx).
		Joins("inner join user_games ug on ug.game_id = games.id").
		Joins("inner join users u on u.id = ug.user_id").
		Preload("Images").
		Preload("HeaderImage").
		Preload("CatalogImage").
		Where("u.id = ?", userID).
		Order("title asc").
		Find(&games).
		Error
}

func FindGameById(id string, tx ...*db.TransactionalOperation) (*g.Game, error) {
	game := &g.Game{}
	return game, db.WithTransaction(tx).Preload("Prices").Where("id = ?", id).First(game).Error
}
func FindGameByIdWithBuilds(id string, tx ...*db.TransactionalOperation) (*g.Game, error) {
	game := &g.Game{}
	return game, db.WithTransaction(tx).
		Preload("Prices").
		Preload("Builds").
		Where("id = ?", id).
		First(game).Error
}
func FindGameByPageUrl(pageUrl string, tx ...*db.TransactionalOperation) (*g.Game, error) {
	game := &g.Game{}
	return game, db.WithTransaction(tx).
		Preload("Prices").
		Preload("Builds").
		Preload("Genres").
		Preload("Images").
		Preload("HeaderImage").
		Preload("CatalogImage").
		Preload("OperatingSystems").
		Where("page_url = ?", pageUrl).
		First(game).Error
}
func ExistsGameByTitle(name string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("title = ?", name).Find(&g.Game{})
	return dbResult.RowsAffected > 0
}
func ExistsGameByPageUrl(pageUrl string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("page_url = ?", pageUrl).Find(&g.Game{})
	return dbResult.RowsAffected > 0
}
func CreateGame(obj *g.Game, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Create(obj).Error
}

func UpdateGame(obj *g.Game, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Model(&g.Game{}).Updates(obj).Error
}

func DeleteGame(id string, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(g.Game{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
