package database

import (
	"fmt"
	"log"
	"math"
	"oppaiman-backend-test/source/config"
	p "oppaiman-backend-test/source/domain/pagination"
	rest "oppaiman-backend-test/source/rest/exceptions"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

func InitDB() {
	var err error
	connectionString := getConnectionString()
	println(connectionString)
	DB, err = gorm.Open(config.GetDatabaseDriver(), connectionString)
	DB.LogMode(true)
	if err != nil {
		log.Panic("An error ocurred during try to connect a database ", err)
	}
}

func getConnectionString() string {
	return fmt.Sprintf(
		"%s:%s@%s",
		config.GetDatabaseUsername(),
		config.GetDatabasePassword(),
		config.GetDatabaseSource(),
	)
}

// Transaction
type TransactionalOperation struct {
	transaction *gorm.DB
}

func UsingTransactional(fn func(*TransactionalOperation) error) {
	err := DB.Transaction(func(tx *gorm.DB) error {
		return fn(&TransactionalOperation{transaction: tx})
	})
	if err != nil {
		except, ok := err.(*rest.HttpException)
		if ok {
			panic(except)
		}

		panic(rest.InternalServerException(err.Error()))
	}
}

func WithTransaction(tx []*TransactionalOperation) *gorm.DB {
	for idx, t := range tx {
		if t != nil {
			return tx[idx].transaction
		}
	}

	return DB
}

func PaginateScope(model interface{}, pagination *p.Pagination, db *gorm.DB) func(db *gorm.DB) *gorm.DB {
	totalRows := uint64(0)
	db.Model(model).Count(&totalRows)

	pagination.TotalRows = totalRows
	pagination.TotalPages = uint(math.Ceil(float64(totalRows) / float64(pagination.GetLimit())))

	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Order(pagination.GetSort())
	}
}
