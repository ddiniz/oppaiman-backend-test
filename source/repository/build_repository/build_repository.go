package build_repository

import (
	b "oppaiman-backend-test/source/domain/build"
	db "oppaiman-backend-test/source/repository/database"

	"gorm.io/gorm"
)

func ListBuilds(tx ...*db.TransactionalOperation) ([]b.Build, error) {
	builds := []b.Build{}
	return builds, db.WithTransaction(tx).Order("created_at desc").Find(&builds).Error
}

func FindBuildById(id uint, tx ...*db.TransactionalOperation) (*b.Build, error) {
	build := &b.Build{}
	return build, db.WithTransaction(tx).Where("id = ?", id).First(build).Error
}
func ExistsBuildByName(name string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("name = ?", name).Find(&b.Build{})
	return dbResult.RowsAffected > 0
}
func ExistsBuildByVersion(name string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("version = ?", name).Find(&b.Build{})
	return dbResult.RowsAffected > 0
}

func CreateBuild(build *b.Build, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Create(build).Error
}

func UpdateBuild(build *b.Build, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Model(&b.Build{}).Updates(build).Error
}

func DeleteBuild(id uint, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(b.Build{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
