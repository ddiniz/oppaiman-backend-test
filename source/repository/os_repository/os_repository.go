package os_repository

import (
	"oppaiman-backend-test/source/domain/game_os"
	db "oppaiman-backend-test/source/repository/database"

	"gorm.io/gorm"
)

func ListOperatingSystems(tx ...*db.TransactionalOperation) ([]game_os.OperatingSystem, error) {
	oses := []game_os.OperatingSystem{}
	return oses, db.WithTransaction(tx).Order("id asc").Find(&oses).Error
}

func FindOperatingSystemById(id uint, tx ...*db.TransactionalOperation) (*game_os.OperatingSystem, error) {
	os := &game_os.OperatingSystem{}
	return os, db.WithTransaction(tx).Where("id = ?", id).First(os).Error
}
func FindOperatingSystemByName(name string, tx ...*db.TransactionalOperation) (*game_os.OperatingSystem, error) {
	os := &game_os.OperatingSystem{}
	return os, db.WithTransaction(tx).Where("name = ?", name).First(os).Error
}

func ExistsOperatingSystemByName(name string, tx ...*db.TransactionalOperation) bool {
	dbResult := db.WithTransaction(tx).Where("name = ?", name).Find(&game_os.OperatingSystem{})
	return dbResult.RowsAffected > 0
}

func CreateOperatingSystem(os *game_os.OperatingSystem, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Create(os).Error
}

func UpdateOperatingSystem(os *game_os.OperatingSystem, tx ...*db.TransactionalOperation) error {
	return db.WithTransaction(tx).Model(&game_os.OperatingSystem{}).Updates(os).Error
}

func DeleteOperatingSystem(id uint, tx ...*db.TransactionalOperation) error {
	dbResult := db.WithTransaction(tx).Delete(game_os.OperatingSystem{ID: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
