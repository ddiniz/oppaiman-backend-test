package file_repository

import (
	"errors"
	"log"
	"os"
	"path/filepath"
)

var PUBLIC_FOLDER = "Media"
var PROTECTED_FOLDER = "Protected"

func GetApplicationPath() (string, error) {
	folder := GetExecutablePath() //This to find if debug used
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		return folder, nil
	}

	folder = GetCurrentDir() //This to find if go run used
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		return folder, nil
	}

	return "", errors.New("couldn't find current folder")

}

func GetOrCreatePublicFolder(path string) (string, error) {
	appPath, err := GetApplicationPath()
	if err != nil {
		return "", err
	}
	folder := appPath + "/" + PUBLIC_FOLDER + "/" + path

	if err := os.Mkdir(folder, os.ModePerm); err != nil {
		return "", err
	}
	return folder, nil
}

func GetOrCreateProtectedFolder(path string) (string, error) {
	appPath, err := GetApplicationPath()
	if err != nil {
		return "", err
	}
	folder := appPath + "/" + PROTECTED_FOLDER + "/" + path

	if err := os.Mkdir(folder, os.ModePerm); err != nil {
		return "", err
	}
	return folder, nil
}

func GetProtectedFile(path string) (*os.File, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func DeleteFolder(path string) error {
	return os.RemoveAll(path)
}

func GetFile(path string) []byte {
	appPath, err := GetApplicationPath()
	if err != nil {
		log.Fatal(err)
	}
	folder := appPath + "/" + PUBLIC_FOLDER + "/" + path
	if _, err := os.Stat(folder); os.IsNotExist(err) {
		log.Fatalf("Unable to locate file {%s}", path)
	}

	dat, err := os.ReadFile(folder + path)
	if err != nil {
		log.Fatalf("Unable to locate file {%s}", path)
		return nil
	}
	return dat
}

func GetCurrentDir() string {
	path, err := os.Getwd()
	if err != nil {
		log.Fatalf("Unable to open current dir with err: %s", err)
	}
	return path
}

func GetExecutablePath() string {
	ex, err := os.Executable()
	if err != nil {
		log.Fatalf("Unable to locate executable current dir with err: %s", err)
	}
	return filepath.Dir(ex)
}
