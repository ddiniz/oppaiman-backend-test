package migrations

import (
	"log"
	"oppaiman-backend-test/source/domain/build"
	"oppaiman-backend-test/source/domain/game"
	"oppaiman-backend-test/source/domain/game_os"
	"oppaiman-backend-test/source/domain/genre"
	"oppaiman-backend-test/source/domain/image"
	"oppaiman-backend-test/source/domain/price"
	"oppaiman-backend-test/source/domain/user"
	"oppaiman-backend-test/source/domain/usergame"
	"oppaiman-backend-test/source/repository/database"
	"oppaiman-backend-test/source/repository/genre_repository"
	"oppaiman-backend-test/source/repository/os_repository"
	"oppaiman-backend-test/source/repository/user_repository"
)

func StartMigration() {
	log.Print("Migrating the data...")
	AutoMigrate()
	InsertBaseData()
	log.Print("Migration finished.")
}

func retrieveAll() []interface{} {
	return []interface{}{
		&user.User{},
		&build.Build{},
		&genre.Genre{},
		&game_os.OperatingSystem{},
		&price.Price{},
		&image.Image{},
		&game.Game{},
		&usergame.UserGame{},
	}
}

func AutoMigrate() {
	database.DB.AutoMigrate(retrieveAll()...)
}

func DropAll() {
	database.DB.DropTable(retrieveAll()...)
}

func InsertBaseData() {
	InsertBaseUsers()
	InsertBaseGenres()
	InsertBaseOperatingSystems()
}
func InsertBaseUsers() {
	user_repository.CreateUser(&user.User{
		ID:       "admin",
		Username: "admin",
		Password: "admin",
		Email:    "admin@admin.com",
		Role:     user.ADMIN,
	})
	user_repository.CreateUser(&user.User{
		ID:       "developer",
		Username: "developer",
		Password: "developer",
		Email:    "developer@developer.com",
		Role:     user.DEVELOPER,
	})
	user_repository.CreateUser(&user.User{
		ID:       "normal",
		Username: "normal",
		Password: "normal",
		Email:    "normal@normal.com",
		Role:     user.NORMAL,
	})
}

func InsertBaseGenres() {
	genre_repository.CreateGenre(&genre.Genre{
		Name:        "RPG",
		Description: "role playing game",
	})
	genre_repository.CreateGenre(&genre.Genre{
		Name:        "Action",
		Description: "action game",
	})
	genre_repository.CreateGenre(&genre.Genre{
		Name:        "FPS",
		Description: "first person shooter",
	})
}
func InsertBaseOperatingSystems() {
	os_repository.CreateOperatingSystem(&game_os.OperatingSystem{
		Name:        "Win7",
		Description: "Windows 7",
	})
	os_repository.CreateOperatingSystem(&game_os.OperatingSystem{
		Name:        "Win8",
		Description: "Windows 8",
	})
	os_repository.CreateOperatingSystem(&game_os.OperatingSystem{
		Name:        "Win10",
		Description: "Windows 10",
	})
	os_repository.CreateOperatingSystem(&game_os.OperatingSystem{
		Name:        "Win11",
		Description: "Windows 11",
	})
	os_repository.CreateOperatingSystem(&game_os.OperatingSystem{
		Name:        "Linux",
		Description: "Linux",
	})
	os_repository.CreateOperatingSystem(&game_os.OperatingSystem{
		Name:        "Mac",
		Description: "MacOperatingSystem",
	})
}
