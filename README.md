# oppaiman-backend-test

docker-compose up

If it doesnt work, do the following:

```
docker run --name oppaiman -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=oppaiman -p 3306:3306 -d mysql
./build.sh --migrate

```

# Prerequisites

Golang 1.20.2

# Running

```
./build.sh --migrate

#OR

go run source/cmd/app/main.go --migrate
```

# Build

```
go build source/cmd/app/main.go
```

# License

Do what you want with the code and assets, just give credit.

Code - Davi Diniz 
